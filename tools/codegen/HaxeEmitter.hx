import haxe.rtti.CType;

using StringTools;

typedef ClassPath = String;
typedef InlineMethodEmitter = GeneratedFile -> ClassPath -> ClassField -> Void;

class HaxeEmitter {

  private static var COMMENT_FIELD_META: String = "##comment";
  private static var META_EXCLUDED_IN_MACROS: Array<String> = [":autoBuild", ":build"];

  private var pathFixes: Map<String, String>;
  // field > class > emitter
  private var inlineMethodEmitters: Map<String, WildcardMap<InlineMethodEmitter>>;
  // param > class field > constraint
  private var typeParamsConstraints: Map<String, WildcardMap<Null<String>>>;

  public function new(rtti: Map<String, TypeTree>) {
    pathFixes = makePathFixesMap(rtti);
    inlineMethodEmitters = new Map();
    typeParamsConstraints = new Map();
  }

  public static function commentField(field: haxe.rtti.ClassField): haxe.rtti.ClassField {
    field.meta = field.meta.copy();
    field.meta.unshift({ name: COMMENT_FIELD_META, params: [] });
    return field;
  }

  public function addPathFix(wrong: Path, fixed: Path) {
    pathFixes.set(wrong, fixed);
  }

  private inline function inlineMethodKey(field: String, isStatic: Bool): String {
    return (isStatic ? "static#" : "inst#") + field;
  }

  public function addInlineMethodEmitter(cls: Null<ClassPath>, field: String, isStatic: Bool, emitter: InlineMethodEmitter): Void {
    var key = inlineMethodKey(field, isStatic);
    var byClass = inlineMethodEmitters.get(key);
    if (byClass == null)
      byClass = inlineMethodEmitters[key] = new WildcardMap();

    byClass.set(cls, emitter);
  }

  private function getInlineMethodEmitter(cls: ClassPath, field: String, isStatic: Bool): Null<InlineMethodEmitter> {
    var key = inlineMethodKey(field, isStatic);
    var byClass = inlineMethodEmitters.get(key);
    return byClass == null ? null : byClass.get(cls);
  }

  // Because type parameter constraints aren't present in RTTI structures, we need to
  // specify them ourselves.
  public function addTypeParamConstraint(classField: Null<String>, param: String, constraint: String): Void {
    var byField = typeParamsConstraints.get(param);
    if (byField == null)
      byField = typeParamsConstraints[param] = new WildcardMap();
    byField.set(classField, constraint);
  }

  private function getTypeParamConstraint(classField: String, param: String): Null<String> {
    var byField = typeParamsConstraints.get(param);
    return byField == null ? null : byField.get(classField);
  }

  public function emitFile(type: TypeTree): GeneratedFile {
    switch (type) {
      case TPackage(name, full, subs):
        throw 'Can\'t generate file for package \'$full\'';

      case TClassdecl(c):
        var file = createFileStart(c, c.isInterface ? "interface" : "class", c.isExtern);
        if (c.superClass != null) {
          file.push(" extends " + fixPath(c.superClass.path));
          emitTypeParameters(file, c.superClass.params);
        }
        
        for (iface in c.interfaces) {
          file.push(" implements " + fixPath(iface.path));
          emitTypeParameters(file, iface.params);
        }

        if (c.tdynamic != null)
          throw "Not implemented: class with dynamic type";
        
        file.push(" {");
        file.indent();
        for (field in c.fields) {
          file.line();
          emitClassField(file, c.path, field, false, false);
        }

        for (field in c.statics) {
          file.line();
          emitClassField(file, c.path, field, false, true);
        }
        file.dedent("}");
        return file;

      case TTypedecl(t):
        var file = createFileStart(t, "typedef", false);
        file.push(" = ");
        if (emitType(file, t.type, false))
          file.push(";");

        return file;

      case TEnumdecl(e):
        throw 'TEnumdecl: not implemented!';

      case TAbstractdecl(a):
        throw 'TAbstractdecl: not implemented!';
    };
  }

  private function createFileStart(type: TypeInfos, kind: String, isExtern: Bool): GeneratedFile {
    var name = type.path.split(".").pop();
    var file = new GeneratedFile(type.module == null ? type.path : type.module);

    emitDocComment(file, type.doc);
    emitMetadata(file, type.meta);

    file.line(type.isPrivate ? "private " : "");
    file.push(isExtern ? "extern " : "");
    file.push(kind + " " + name);
    emitTypeParamDecls(file, type.path + ".<type>", type.params);
    return file;
  }

  private function emitDocComment(file: GeneratedFile, doc: Null<String>, isMultiline: Bool = true): Void {
    if (doc == null) {
      return;
    }

    if (isMultiline) {
      file.line("/**");
      for (line in doc.split("\n")) {
        file.line(line.trim());
      }
      file.line("**/");
    } else {
      for (line in doc.split("\n")) {
        file.line("// ");
        file.push(line.trim());
      }
    }
  }

  private function emitMetadata(file: GeneratedFile, meta: MetaData): Void {
    for (item in meta) {
      file.line();
      var excludedInMacros = META_EXCLUDED_IN_MACROS.indexOf(item.name) >= 0;
      if (excludedInMacros) {
        file.push("#if !macro ");
      }

      file.push("@" + item.name);
      if (item.params.length > 0) {
        file.push("(" + item.params.join(", ") + ")");
      }

      if (excludedInMacros) {
        file.push(" #end");
      }
    }
  }

  private function emitTypeParamDecls(file: GeneratedFile, classField: String, params: Array<String>): Void {
    if (params.length == 0)
      return;
    var sep = "<";

    for (p in params) {
      file.push(sep);
      sep = ", ";

      var constraint = getTypeParamConstraint(classField, p);
      if (constraint == null)
        throw 'Unknown type parameter \'$p\' for field \'$classField\'';

      file.push(p);
      if (constraint != "") {
        file.push(": ");
        file.push(constraint);
      }
    }

    file.push(">");
  }

  private function emitClassField(file: GeneratedFile, cls: ClassPath, field: ClassField, compact: Bool, isStatic: Bool = false): Void {
    if (compact) {
      throw "Not implemented: compact class fields";
    } else {
      if (field.overloads != null)
        throw "Not implemented: field overloads";

      var isFieldCommented = field.meta.length > 0 && field.meta[0].name == COMMENT_FIELD_META;
      emitDocComment(file, field.doc, !isFieldCommented);
      if (isFieldCommented) {
        file.line("// ");
      } else {
        emitMetadata(file, field.meta);
        file.line();
      }
      file.push(field.isOverride ? "override " : "");
      file.push(field.isPublic ? "public " : "private ");
      file.push(isStatic ? "static " : "");

      switch [field.get, field.set] {
        case [RNormal, RNormal]: // normal field
          if (field.params.length > 0)
            throw "Not implemented: parametric fields";
          file.push("var " + field.name + ": "); 
          emitType(file, field.type, true);
          file.push(";");

        case [RNormal, RMethod | RDynamic]: // method
          file.push("function " + field.name);
          // We don't care about exact generic types on commented fields.
          if (!isFieldCommented)
            emitTypeParamDecls(file, cls + "." + field.name, field.params);
          emitType(file, field.type, false);
          file.push(";");

        case [RInline, RNo]: // inline method
          if (isFieldCommented) {
            file.push("function " + field.name);
            // We don't care about exact generic types on commented fields.
            emitType(file, field.type, false);
            file.push(";");
          } else {
            var emitter = getInlineMethodEmitter(cls, field.name, isStatic);
            if (emitter == null)
              throw 'No inline method emitter for \'$cls.${field.name}\'';

            file.push("inline function " + field.name);
            emitTypeParamDecls(file, cls + "." + field.name, field.params);
            emitType(file, field.type, false);
            file.indent(" {");
            emitter(file, cls, field);
            file.dedent("}");
          }

        default:
          throw 'Not implemented: field rights [${field.get}, ${field.set}] for \'$cls.${field.name}\'';
      }
    }
  }

  private function emitTypeParameters(file: GeneratedFile, types: List<CType>) {
    if (types.length > 0) {
      var sep = "";
      file.push("<");
      for (type in types) {
        file.push(sep);
        sep = ", ";
        emitType(file, type, true);
      }
      file.push(">");
    }
  }

  private function emitType(file: GeneratedFile, type: CType, compact: Bool): Bool {
    switch (type) {
      case CAbstract(path, params) | CTypedef(path, params) | CClass(path, params):        
        file.push(fixPath(path));
        emitTypeParameters(file, params);

      case CAnonymous(fields):
        if (compact)
          file.push("{ ");
        else file.indent("{");

        for (field in fields) {
          emitClassField(file, "<anon>", field, compact);
          if (compact)
            file.push(", ");
        }

        if (compact)
          file.push(" }");
        else file.dedent("}");

        return compact;

      case CFunction(args, ret):
        if (compact && args.length == 0) {
          args.add({ name: "", opt: false, t: CAbstract("Void", new List()) });
        }

        file.push(compact ? "" : "(");
        var sep = "";
        for (arg in args) {
          file.push(sep);
          sep = compact ? " -> " : ", ";

          if (arg.opt) {
            if (compact)
              throw "Not supported: optional args in compact fonction types";
            file.push("?");
          }

          file.push(compact ? "" : (arg.name + ": "));
          emitType(file, arg.t, true);

          if (arg.value != null) {
            if (compact)
              throw "Not supported: default values in compact fonction types";
            file.push(" = " + arg.value);
          }
        }

        file.push(compact ? " -> " : "): ");
        emitType(file, ret, true);

      case CDynamic(inner):
        file.push("Dynamic");
        if (inner != null) {
          file.push("<");
          emitType(file, inner, true);
          file.push(">");
        }

      default:
        throw "Type not supported: " + type;
    }

    return true;
  }

  private function fixPath(path: Path): Path {
    var fixed = pathFixes.get(path);
    if (fixed != null)
      return fixed;

    // Detect generic parameters: they look like `foo.bar.Baz.T` or `methodName.T`;
    // we suppose they are a single char.
    var lastDot = path.lastIndexOf(".");
    if (lastDot < path.length - 2)
      return path;

    trace("generic found: " + path);
    return path.substring(lastDot + 1);
  }

  private static function makePathFixesMap(rtti: Map<String, TypeTree>): Map<String, String> {
    var fixes = new Map();

    // Types in submodules aren't properly specified in the RTTI structures.
    // For example, `hf.Data.ItemInfo` is stored as `hf.ItemInfo`.
    // We need to build a map of these wrong paths to be able to fix them later.
    for (item in rtti) {
      if (item.match(TPackage(_, _, _)))
        continue;
      
      var type = TypeApi.typeInfos(item);
      if (type.isPrivate || type.module == null || type.module == "StdTypes")
        continue;

      var truePath = type.module + "." + type.path.substring(type.path.lastIndexOf(".") + 1);
      fixes.set(type.path, truePath);
    }

    return fixes;
  }
}
