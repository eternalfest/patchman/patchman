
class WildcardMap<T> {
  private var wildcard: Null<T> = null;
  private var hasWildcard: Bool = false;
  private var byKey: Map<String, T>;

  public function new() {
    this.byKey = new Map();
  }

  public function get(key: Null<String>): Null<T> {
    if (key != null) {
      var v = this.byKey[key];
      if (v != null || this.byKey.exists(key))
        return v;
    }
    return this.wildcard;
  }

  public function set(key: Null<String>, val: T): T {
    if (key == null) {
      this.wildcard = val;
      this.hasWildcard = true;
    } else {
      this.byKey[key] = val;
    }
    return val;
  }
}
