import haxe.io.Path;

class PatchmanRefs {

  private var patchesPack(default, null): String;
  private var refPack(default, null): String;
  private var nilType(default, null): String;
  private var hfType(default, null): String;

  private function new(patchesPack: String, refPack: String, nilType: String, hfType: String): Void {
    this.patchesPack = patchesPack;
    this.refPack = refPack;
    this.nilType = nilType;
    this.hfType = hfType;
  }

  public static function generate(patchesPack: String, refPack: String, nilType: String, hfType: String, maxArgs: Int): Array<GeneratedFile> {
    var gen = new PatchmanRefs(patchesPack, refPack, nilType, hfType);
    var files = [];

    for (args in 0...(maxArgs+1)) {
      files.push(gen.generateFile("Function" + args, false, true, args));
      files.push(gen.generateFile("Method" + args, true, true, args));
      files.push(gen.generateFile("FunctionVoid" + args, false, false, args));
      files.push(gen.generateFile("MethodVoid" + args, true, false, args));
    }

    return files;
  }

  private function generateFile(name: String, hasCtx: Bool, hasRetType: Bool, nbArgs: Int): GeneratedFile {
    var file = new GeneratedFile(refPack + "." + name);

    function generateFunction(fnName: String, patchName: String, fnType: String): Void {
      file.line();
      file.indent('public function $fnName(fn: $fnType): IPatch {');
      file.line('return new $patchName(this.parentPath, this.name, ${hasCtx ? "true" : "false"}, $nbArgs, fn);');
      file.dedent('}');
    }

    var retType = hasRetType ? "R" : "Void";
    var args = [for (i in 0...nbArgs) ("A" + i)];
    if (hasCtx)
      args.unshift("C");
    var classTypes = hasRetType ? args.concat([retType]) : args;

    file.line('import $patchesPack.AfterPatch;');
    file.line('import $patchesPack.BeforePatch;');
    file.line('import $patchesPack.PostfixPatch;');
    file.line('import $patchesPack.PrefixPatch;');
    file.line('import $patchesPack.ReplacePatch;');
    file.line('import $patchesPack.WrapPatch;');
    file.line('import ${patchesPack.split(".")[0]}.IPatch;');
    file.line('import $nilType;');
    file.line();
    file.indent('class $name<${classTypes.join(", ")}> extends Ref {');
    
    file.indent(  "public function new(path: Array<String>) {");
    file.line(       hasCtx ? "super(path, false);" : "super(path, true);");
    file.dedent(  "}");

    var fnArgs = [hfType].concat(args);
    var fnArgsWithRet = hasRetType ? fnArgs.concat([retType]) : fnArgs;

    generateFunction("before", "BeforePatch", makeFunctionType(fnArgs));
    generateFunction("prefix", "PrefixPatch", makeFunctionType(fnArgs, '${nilType.split(".").pop()}<$retType>'));
    generateFunction("wrap", "WrapPatch", makeFunctionType(
      fnArgs.concat(["(" + makeFunctionType(args, retType) + ")"]),
      retType
    ));
    generateFunction("replace", "ReplacePatch", makeFunctionType(fnArgs, retType));
    generateFunction("postfix", "PostfixPatch", makeFunctionType(fnArgsWithRet, retType));
    generateFunction("after", "AfterPatch", makeFunctionType(fnArgsWithRet));

    file.dedent('}');

    return file;
  }

  private static function makeFunctionType(args: Array<String>, ?ret: String): String {
    return (args.length == 0 ? "Void" : args.join(" -> ")) + " -> " + (ret == null ? "Void" : ret);
  }
}
