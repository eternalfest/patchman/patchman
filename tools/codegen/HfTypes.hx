import haxe.rtti.CType;
import HaxeEmitter.ClassPath;

using StringTools;

class HfTypes {

  public static function extractStaticTypes(rtti: Map<String, TypeTree>, pack: String, rootClass: String, autoBuild: String): Array<GeneratedFile> {
    var packPrefix = pack + ".";

    var rootClassBase: Null<Classdef> = null;
    var rootClassGen: Null<Classdef> = null;

    var outTypes = [];
    for (name in rtti.keys()) {
      if (!(name == pack || name.startsWith(packPrefix)))
        continue;

      var type = rtti.get(name);

      switch (type) {
        case TPackage(_, _, subs):
          if(!isRealPackage(subs))
            continue;

          switch (createPackageType(name, subs)) {
            case TClassdecl(c) if (name == pack):
              rootClassGen = c;
            case out:
              outTypes.push(out);
          }

        case TClassdecl(c):
          if (name == rootClass) {
            rootClassBase = c;
          } else if (hasInterfaceMeta(c)) {
            outTypes.push(type);
          } else {
            var superClassInPackage = c.superClass != null && c.superClass.path.startsWith(packPrefix);
            outTypes.push(createStaticType(c));
            outTypes.push(createReducedClassType(c, rootClass, superClassInPackage ? null : autoBuild));
          }

        default:
          outTypes.push(type);
      }
    }

    if (rootClassBase == null || rootClassGen == null)
      throw "HfTypes: couldn't find root class and/or package";
    rootClassBase = Reflect.copy(rootClassBase);
    rootClassBase.fields = rootClassGen.fields;
    rootClassBase.doc = concatDocStrings(rootClassBase.doc, rootClassGen.doc);
    outTypes.push(TClassdecl(rootClassBase));

    var emitter = new HaxeEmitter(rtti);
    CodeEmitters.configureEmitter(emitter, pack);
    return [for (type in outTypes) emitter.emitFile(type)];
  }

  // Returns the name of the type representing the given package.
  // For example, `foo.bar.baz.quux` becomes `foo.pack.bar.baz.Quux`.
  private static function makePackageTypeName(pack: String): String {
    var parts = pack.split(".");
    parts.insert(1, "pack");
    
    var last = parts.pop();
    parts.push(last.charAt(0).toUpperCase() + last.substring(1));
    return parts.join(".");
  }

  // Returns the name of the type representing the static members of the given class.
  // For example, `foo.bar.baz.Quux` becomes `foo.classes.bar.baz.Quux`.
  private static function makeStaticTypeName(path: String): String {
    var parts = path.split(".");
    parts.insert(1, "classes");
    return parts.join(".");
  }

  // Creates a type representing the given package.
  // It has a field for each contained class or subpackage.
  private static function createPackageType(pack: String, subs: Array<TypeTree>): TypeTree {
    var classdef = makeSimpleGeneratedClass(makePackageTypeName(pack), 'The contents of the `$pack` package.');

    for (sub in subs) {
      var type: CType;
      var path: String;
      switch (sub) {
        case TPackage(name, full, packsubs):
          if (!isRealPackage(packsubs))
            continue; // ignore

          type = CClass(makePackageTypeName(full), new List());
          path = full;

        case TClassdecl(c):
          if (c.module != null)
            throw 'HfTypes: Unexpected type in submodule: ${c.path}';

          if (hasInterfaceMeta(c))
            continue; // ignore

          type = CClass(makeStaticTypeName(c.path), new List());
          path = c.path;

        case TEnumdecl(_):
          throw "HfTypes: enums not supported";

        case TAbstractdecl(_):
          throw "HfTypes: abstract not supported";

        case TTypedecl(_):
          continue; // ignore
      }

      classdef.fields.add(makeSimpleClassField(path.split(".").pop(), type));
    }

    return TClassdecl(classdef);
  }

  // Creates a type containing the static fields (as instance fields) of the given class.
  // An inline method named `_class` is generated, to allow casting to a `Class<T>` instance.
  // If the class has a constructor, an inline method named `_new` is also generated.
  private static function createStaticType(decl: Classdef): TypeTree {
    if (decl.params.length > 0)
      throw 'HfTypes: parametric classes ${decl.path} isn\'t supported.';

    var statics = makeSimpleGeneratedClass(makeStaticTypeName(decl.path), 'The static fields of the `${decl.path}` class.');
    statics.fields = decl.statics.map(function(e) return e);

    var declType = CClass(decl.path, new List());
    var classType = {
      var params = new List();
      params.add(CClass(decl.path, new List()));
      CClass("Class", params);
    };

    statics.fields.add(makeInlineClassField("_class", CFunction(new List(), classType), "Get this class as a `Class<T>` instance."));

    for (field in decl.fields) {
      if (field.name != "new" || !field.isPublic)
        continue;

      switch(field.type) {
        case CFunction(args, ret): statics.fields.add(makeInlineClassField("_new", CFunction(args, declType), field.doc));
        default: continue;
      }
    }

    return TClassdecl(statics);
  }

  // Creates a reduced version of the given class:
  //  - it doesn't contain any static fields;
  //  - all member fields are annotated with `:keep`;
  //  - its constructor is marked private and has an argument of
  //    type `rootClass` prepended to its argument list;
  //  - if 'autoBuild' is specified, an `@:autoBuild` annotation
  //    is added with the given content.
  private static function createReducedClassType(decl: Classdef, rootClass: String, ?autoBuild: String): TypeTree {
    var reduced = Reflect.copy(decl);

    var staticTypeName = makeStaticTypeName(decl.path);

    if (reduced.statics.length > 0)
      reduced.doc = concatDocStrings(reduced.doc, 'The static fields of this class can be accessed from `$staticTypeName`.');

    reduced.statics = decl.statics.map(function(field) return HaxeEmitter.commentField(Reflect.copy(field)));

    reduced.fields = decl.fields.map(function(field) {
      field = Reflect.copy(field);

      if (field.name == "new") {
        switch (field.type) {
          case CFunction(args, ret):
            args = args.map(function(e) return e);
            args.push({ name: "hf", opt: false, t: CClass(rootClass, new List()) });
            field.type = CFunction(args, ret);
          default:
            throw 'Expected ${field.name} to be a method';
        };
        field.isPublic = false;
      }

      field.meta.push({ name: ":keep", params: [] });
      return field;
    });

    var accessFnArgs = new List();
    accessFnArgs.add({
      t: CClass(rootClass, new List()),
      name: rootClass.split(".")[0],
      opt: false,
      value: null,
    });
    var staticType = CClass(staticTypeName, new List());
    var classType = {
      var params = new List();
      params.add(CClass(decl.path, new List()));
      CClass("Class", params);
    };
    reduced.statics.push(makeInlineClassField("_statics", CFunction(accessFnArgs, staticType), "Resolves this class' statics on the `hf` root."));
    reduced.statics.push(makeInlineClassField("_class", CFunction(accessFnArgs, classType), "Resolves this class on the `hf` root."));

    if (autoBuild != null)
      reduced.meta.push({ name: ":autoBuild", params: [autoBuild] });

    return TClassdecl(reduced);
  }

  private static function isRealPackage(subs: Array<TypeTree>): Bool {
    for (sub in subs) {
      switch (sub) {
        case TPackage(_, _, subs) if (isRealPackage(subs)): return true;
        case TClassdecl(c) if (!hasInterfaceMeta(c)): return true;
        case TEnumdecl(e) if (!hasInterfaceMeta(e)): return true;
        default: continue;
      }
    }
    return false;
  }

  private static function makeSimpleClassField(name: String, type: CType, ?doc: String): ClassField {
    return {
      name: name,
      type: type,
      get: RNormal,
      set: RNormal,
      doc: doc,
      line: null,
      isOverride: false,
      isPublic: true,
      meta: [],
      overloads: null,
      params: [],
      platforms: new List(),
    };
  }

  private static function makeInlineClassField(name: String, type: CType, ?doc: String): ClassField {
    var field = makeSimpleClassField(name, type, doc);
    field.get = RInline;
    field.set = RNo;
    return field;
  }

  private static function makeSimpleGeneratedClass(path: String, ?doc: String): Classdef {
    return {
      path: path,
      doc: doc,
      fields: new List(),
      file: null,
      interfaces: new List(),
      isExtern: true,
      isInterface: false,
      isPrivate: false,
      meta: [{ name: ":interface", params: [] }],
      module: null,
      params: [],
      platforms: new List(),
      statics: new List(),
      superClass: null,
      tdynamic: null,
    };
  }

  private static function hasInterfaceMeta(decl: TypeInfos): Bool {
    for (meta in decl.meta) {
      if (meta.name == ":interface")
        return true;
    }
    return false;
  }

  private static function concatDocStrings(doc1: Null<String>, doc2: Null<String>): Null<String> {
    if (doc1 == null)
      return doc2;
    else if (doc2 == null)
      return doc1;
    else return doc1 + "\n\n" + doc2;
  }
}

private class CodeEmitters {

  public static function configureEmitter(emitter: HaxeEmitter, pack: String): Void {
    emitter.addInlineMethodEmitter(null, "_class", false, simpleCastMethod);
    emitter.addInlineMethodEmitter(null, "_new", false, staticTypeFakeConstructor);
    emitter.addInlineMethodEmitter(null, "_statics", true, pathAccessMethod);
    emitter.addInlineMethodEmitter(null, "_class", true, pathAccessMethod);
    emitter.addTypeParamConstraint('$pack.Pos.<type>', "T", "");

    // Fix the wierd path generated by 'import hf.levels.Data in LevelData' in sources
    emitter.addPathFix('$pack.levels._Data.LevelData', '$pack.levels.Data');
  }

  private static function staticTypeFakeConstructor(file: GeneratedFile, cls: ClassPath, field: ClassField): Void {
    var args = getMethodArgTypes(field);

    file.line("return untyped __new__(this");
    for (arg in args) {
      file.push(", " + arg.name);
    }
    file.push(");");
  }

  private static function simpleCastMethod(file: GeneratedFile, cls: ClassPath, field: ClassField): Void {
    var args = getMethodArgTypes(field);

    if (args.length != 0)
      throw 'Expected no arguments for static method ${field.name}';

    file.line("return cast this;");
  }

  private static function pathAccessMethod(file: GeneratedFile, cls: ClassPath, field: ClassField): Void {
    var args = getMethodArgTypes(field);
    if (args.length != 1)
      throw 'Expected a single `hf` argument for method ${field.name}';

    file.line('return cast $cls;');
  }

  private static function getMethodArgTypes(field: ClassField): List<FunctionArgument> {
    return switch (field.type) {
      case CFunction(args, r): args;
      default: throw 'Expected ${field.name} to be a method';
    };
  }
}
