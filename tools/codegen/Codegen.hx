import haxe.rtti.CType;

class Codegen {
  public static function main(): Void {
    var haxeXmlPath = getEnv("CODEGEN_XML_PATH");
    var outDir = getEnv("CODEGEN_OUT_DIR");
    var mainPack = getEnv("CODEGEN_MAIN_PACK");

    var rtti = parseRtti(haxeXmlPath);

    var files = [];
    files = files.concat(SuperBindings.generate(rtti, mainPack, "patchman.fpatch.SuperBindingTable"));
    files = files.concat(HfTypes.extractStaticTypes(rtti, mainPack, mainPack + ".Hf", "patchman.Build.hfClass()"));
    files = files.concat(PatchmanRefs.generate("patchman.fpatch", "patchman.ref", "etwin.ds.Nil", mainPack + ".Hf", 9));
    
    for (file in files) {
      file.writeToDisk(outDir);
    }
  }

  private static function parseRtti(path: String): Map<String, TypeTree> {
    function fillRttiMap(rtti: TypeRoot, map: Map<String, TypeTree>): Void {
      for (item in rtti) {
        switch (item) {
          case TPackage(name, full, subs):
            map.set(full, item);
            fillRttiMap(subs, map);

          case TClassdecl(c): map.set(c.path, item);
          case TEnumdecl(e): map.set(e.path, item);
          case TTypedecl(t): map.set(t.path, item);
          case TAbstractdecl(a): map.set(a.path, item);
        }
      };
    }

    var xml = Xml.parse(sys.io.File.getContent(path));

    var rttiParser = new haxe.rtti.XmlParser();
    rttiParser.process(xml.firstChild(), "flash8");

    var map = new Map();
    fillRttiMap(rttiParser.root, map);
    return map;
  }

  private static function getEnv(name: String): String {
    var value = Sys.getEnv(name);
    if (value == null) {
      throw 'Missing env variable: $name';
    }
    return value;
  }
}