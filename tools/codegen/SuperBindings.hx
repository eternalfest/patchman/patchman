import haxe.io.Path;
import haxe.rtti.CType;
import haxe.Json;
import haxe.macro.Expr;

import etwin.ds.Set;

using StringTools;

private enum SuperBindingTarget {
  None;
  Dynamic(cls: String);
  Static(depth: Int);
}

private typedef SuperBindingRegion = {
  path: String,
  field: String,
  targets: Map<String, SuperBindingTarget>,
}

private enum NestedMap<K, V> {
  Value(v: V);
  SubMap(map: Map<K, NestedMap<K, V>>);
}

private typedef BindingsResult = {
  readDyn: NestedMap<String, String>,
  writeDyn: Map<String, Set<String>>,
  writeVar: NestedMap<String, { binding: String, depth: Int }>,
}

class SuperBindings {
  public static function generate(rtti: Map<String, TypeTree>, pack: String, name: String): Array<GeneratedFile> {
    var classes = filterClasses(pack, rtti);
    var overridenNames = getOverridenNames(classes);
    var result = getBindings(pack, classes, overridenNames);

    return [generateClassFile(name, result)];
  }

  private static function filterClasses(pack: String, rtti: Map<String, TypeTree>): Map<String, Classdef> {
    var packPrefix = pack + ".";
    var classes = new Map();
    
    for (type in rtti) {
      switch (type) {
        case TClassdecl(cls) if (cls.path.startsWith(packPrefix)):
          classes.set(cls.path, cls);
        default:
          continue;
      }
    }
    return classes;
  }

  private static function getOverridenNames(classes: Map<String, Classdef>): Map<String, Set<Classdef>> {
    var overridenNames = new Map();

    for (cls in classes) {
      for (field in cls.fields) {
        if (field.isOverride && field.type.match(CFunction(_, _))) {
          getOrInsert(overridenNames, field.name, new Set()).add(cls);
        }
      }
    }
    return overridenNames;
  }

  private static function getBindings(pack: String, classes: Map<String, Classdef>, overridenNames: Map<String, Set<Classdef>>): BindingsResult {
    var readDyn: Map<String, String> = new Map();
    var writeDyn: Map<String, Set<String>> = new Map();
    var writeVar: Map<String, { binding: String, depth: Int }> = new Map();

    for (name in overridenNames.keys()) {
      for (region in getBindingRegions(classes, name, overridenNames.get(name))) {
        var hasEmptyTarget = Lambda.exists(region.targets, function (t) { return t.match(SuperBindingTarget.None); });

        if (!isClassInPackage(region.path, pack)) {
          continue;
        }

        var binding = getRelativeClassPath(pack, region.path).replace(".", "_") + "__" + region.field;
        
        for (classPath in region.targets.keys()) {
          var relativeClassPath = getRelativeClassPath(pack, classPath);
          var fieldFullPath = relativeClassPath + "." + name;
          switch (region.targets.get(classPath)) {
            case None:
              readDyn.set(fieldFullPath, binding);
            case Dynamic(_):
              if (hasEmptyTarget) {
                readDyn.set(fieldFullPath, binding);
                getOrInsert(writeDyn, binding, new Set()).add(relativeClassPath);
              }
            case Static(depth):
              writeVar.set(fieldFullPath, { binding: binding, depth: depth });
          }
        }
      }
    }
    return {
      readDyn: makeNestedMap(readDyn),
      writeDyn: writeDyn,
      writeVar: makeNestedMap(writeVar),
    };
  }

  private static function isClassInPackage(classPath: String, pack: String): Bool {
    return classPath.startsWith(pack + ".");
  }

  private static function getRelativeClassPath(pack: String, classPath: String): String {
    if (!isClassInPackage(classPath, pack)) {
      throw 'RelativeClassPathError: Class $classPath isn\'t in package $pack';
    }
    return classPath.substring(pack.length + 1);
  }

  private static function getBindingRegions(classes: Map<String, Classdef>, name: String, overrides: Set<Classdef>): Array<SuperBindingRegion> {
    var bindings: Map<String, {
      path: String,
      field: String,
      linear: Array<String>,
      overrides: Set<String>,
      all: Set<String>,
    }> = new Map();

    for (cls in overrides) {
      var chain = [];
      var cur = cls;

      while (true) {
        if (cur.superClass == null) {
          throw 'BindingResolutionFailure: class ${cur.path} has no super class (for ${cls.path}.$name)';
        }
        var parentPath = cur.superClass.path;
        var parent = classes.get(cur.superClass.path);
        chain.unshift(parentPath);

        if (parent != null && !Lambda.exists(parent.fields, function(field) { return field.name == name; })) {
          cur = parent;
          continue;
        }

        var region = getOrInsert(bindings, '${parentPath}#$name', ({
          path: parentPath,
          field: name,
          linear: chain,
          all: Set.from(chain),
          overrides: new Set(),
        }));

        var matching = 0;
        for (i in 0...chain.length) {
          region.all.add(chain[i]);
          if (i < (region.linear: Array<String>).length && chain[i] == region.linear[i]) {
            matching = i + 1;
          }
        }
          
        region.linear.splice(matching, region.linear.length);
        region.all.add(cls.path);
        region.overrides.add(cls.path);
        break;
      }
    }

    var result = [];
    for (region in bindings) {
      var targets = new Map<String, SuperBindingTarget>();
      for (classPath in region.all) {
        var linearIndex = region.linear.indexOf(classPath);
        targets.set(classPath,
          if (linearIndex >= 0) {
            Static(linearIndex);
          } else if (region.overrides.exists(classPath)) {
            Dynamic(classes.get(classPath).superClass.path);
          } else {
            None;
          });
      }

      result.push({
        path: region.path,
        field: region.field,
        targets: targets,
      });
    }

    return result;
  }

  private static function generateClassFile(name: String, bindings: BindingsResult): GeneratedFile {
    var file = new GeneratedFile(name);

    function emitReadDyn(readDyn: NestedMap<String, String>): Void {
      emitNestedObject(file, "return ", readDyn, ";", function (v){
        return Json.stringify(v);
      });
    }

    function emitWriteDyn(writeDyn: Map<String, Set<String>> ): Void {
      file.indent("return [");
      for (key in sortStrings(writeDyn.keys())) {
        file.indent(Json.stringify(key) + " => [");
        for (classPath in sortStrings(writeDyn.get(key).iterator())) {
          var elems = classPath.split(".").map(function(s) { return Json.stringify(s); });
          file.line('[${elems.join(", ")}],');
        }
        file.dedent("],");
      }
      file.dedent("];");
    }

    function emitWriteVar(writeVar: NestedMap<String, { binding: String, depth: Int }>): Void {
      emitNestedObject(file, "return ", writeVar, ";", function (v) { 
        return '([${Json.stringify(v.binding)},${v.depth}]: Array<Dynamic>)';
      });
    }

    file.indent("class SuperBindingTable {");
    file.line  ("");
    file.line  (  "public static var READ_DYN: Dynamic = createReadDyn();");
    file.indent(  "private static function createReadDyn(): Dynamic {");
    emitReadDyn(bindings.readDyn);
    file.dedent(  "}");
    file.line  ("");
    file.line  (  "public static var WRITE_DYN: Dynamic = createWriteDyn();");
    file.indent(  "private static function createWriteDyn(): Dynamic {");
    emitWriteDyn(bindings.writeDyn);
    file.dedent(  "}");
    file.line  ("");
    file.line  (  "public static var WRITE_VAR: Dynamic = createWriteVar();");
    file.indent(  "private static function createWriteVar(): Dynamic {");
    emitWriteVar(bindings.writeVar);
    file.dedent(  "}");
    file.dedent("}");
    
    return file;
  }

  private static function emitNestedObject<V>(file: GeneratedFile, prefix: String, object: NestedMap<String, V>, postfix: String, stringify: V -> String): Void {
    switch(object) {
      case Value(v):
        file.line(prefix + stringify(v) + postfix);
      case SubMap(map):
        file.indent(prefix + "{");
        for (key in sortStrings(map.keys())) {
          emitNestedObject(file, key + ": ", map.get(key), ",", stringify);
        }
        file.dedent("}" + postfix);
    }
  }

  private static function makeNestedMap<V>(map: Map<String, V>): NestedMap<String, V> {
    var root = new Map();
    var out: NestedMap<String, V> = SubMap(root);

    for (key in map.keys()) {
      var parts = key.split(".");
      if (parts.length == 0) {
        throw "NestedMapError: keys can't be empty";
      }
      var last = parts.pop();

      var cur = root;
      for (part in parts) {
        switch (getOrInsert(cur, part, SubMap(new Map()))) {
          case Value(_): throw 'NestedMapError: Key $key overlaps with an existing value';
          case SubMap(map): cur = map;
        }
      }

      if (cur.exists(last)) {
        throw throw 'NestedMapError: Key $key overlaps with an existing value';
      }
      cur.set(last, Value(map.get(key)));
    }

    return out;
  }

  private static function sortStrings(strings: Iterator<String>): Array<String> {
    var array = [for (s in strings) s];
    array.sort(Reflect.compare);
    return array;
  }

  private static macro function getOrInsert<K, V>(map: ExprOf<Map<K, V>>, key: ExprOf<K>, insert: ExprOf<V>): ExprOf<V> {
    return macro {
      var __map = $map;
      var __key = $key;
      var __val = __map.get(__key);
      if (__val == null) {
        __val = $insert;
        __map.set(__key, __val);
      }
      __val;
    }
  }
}