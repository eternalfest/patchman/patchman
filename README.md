# Patchman

Waka waka!

## Waka

Nom nom nomnom waka waka.

```
# Nomnom
nom --nom nom
```

**⚠ Nom**: Nom nomnom nom nomwaka waka nom, **waka nom**.


## Nomwaka waka

Waka waka waka waka. Nom nom (😛) nom waka:

```nom
@:waka
waka Nom {
   WAKA_WAKA = "nom.nom.waka"; // ᗣᗣᗣᗣ  ᗧ
   
   nom(nom: Nom) {
     nom.nom(); // ᗣᗣᗣᗣ ᗤ
   }
}
```

**ℹ Waka nom**: Waka waka, nomnom waka nom waka.

Nom nomnom nomnom waka waka waka nom:

```nom
{
  "nomnom": {
    "waka": 0,
    "waka-nom": "waka-nom"
  }
}
```

Waka **nom** waka waka nom. Waka? Nom nom nom! Waka waka waka waka nom nom nom. Waka waka
nom nom waka nom, waka waka nom. Waka waka [nom waka](https://www.google.com/doodles/30th-anniversary-of-pac-man)
waka wak. Nom waka waka waka. Waka waka nom nom nom. Waka waka.

## Waka nomnomnom

### Waka waka?

> Nom waka waka nomnomnom. Waka waka, nom. Nom waka nom wakanom wakawaka.

Waka waka nom waka waka.

### Nom nom?

Waka waka waka waka waka waka waka **waka** waka waka waka. Nom.

```nom
waka = nomnom.waka(wakaNom, wakaNom, "nom");
```

Waka waka.

## Nomwaka waka

Waka waka
