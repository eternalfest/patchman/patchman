# Next

- **[Breaking change]** Fully remove deprecated `Ref.get` macro.
- **[Change]** Do not print Haxe arguments to the console unless `--verbose` is specified.
- **[Change]** Define a Haxe preprocessor variable for each Patchman dependency:
  - The `patchman` define is set to the current Patchman version;
  - If `@patchman/foo-bar` exists in the dependency tree, `foo_bar` is set to its current version.
- **[Fix]** Fix doc-comment syntax in source files.
- **[Internal]** Refactor `patchman.di` internals.

# 0.10.12 (2023-03-04)

- **[Feature]** Add two convenience static methods to `hf.*` types:
  - `function _class(hf: hf.Hf): Class<hf.*>`;
  - `function _statics(hf: hf.Hf): hf.classes.*`.
- **[Fix]** Update dependencies.

# 0.10.11 (2021-12-14)

- **[Breaking change]** Update to native ESM, requires Node 14.13.1 or higher.
- **[Fix]** Update dependencies.

# 0.10.8 (2021-12-13)

- **[Feature]** Add support to generic methods in `patchman.Ref`.
- **[Fix]** Prevent constructors of `@:hfTemplate`d classes from being removed by DCE.
- **[Fix]** Fix compiler stack overflow on packages with many `Build.di()` classes.

# 0.10.7 (2021-04-26)

- **[Fix]** Always call obfuscated methods on all loader host modules.

# 0.10.6 (2021-04-23)

- **[Fix]** Prevent DCE from removing methods in `:hfTemplate` classes.

# 0.10.5 (2021-04-22)

- **[Fix]** Update to `@etwin-haxe/core` version `0.1.5`.

# 0.10.4 (2021-04-21)

- **[Fix]** Update to `@etwin-haxe/core` version `0.1.4`.

# 0.10.3 (2021-04-20)

- **[Fix]** Update to `@etwin-haxe/core` version `0.1.3`.

# 0.10.2 (2021-04-20)

- **[Internal]** Update to Yarn 2.

# 0.10.1 (2021-04-20)

- **[Fix]** Fix borked previous release.

# 0.10.0 (2021-04-20)

- **[Breaking Change]** Implement new package resolution algorithm:
  - Dependencies can now be defined with the `dependency` field (instead of `peerDependencies`)
  - Enabled dependency uniqueness check
  - Allow libraries to declare their compatibility with Patchman versions using `engines.patchman`.
- **[Breaking Change]** Remove types `{patchman, ef}.{Symbol, ds.*}` use `etwin.{Symbol, ds.*}` types instead.
- **[Breaking Change]** Move `patchman.Obfu` to `etwin.Obfu`.
- **[Change]** Enable full DCE for Haxe compilation.
- **[Feature]** Add `patchman --doc` to generate documentation using `dox`.
  - The output directory can be configured in the `package.json` at `patchman.outDocDir`.
- **[Feature]** Add `Obfu.call` macro.
- **[Feature]** `Obfu.raw` macro now accepts object literals.
- **[Feature]** Multiple source directories can be specified in the `package.json` at `patchman.haxe` by giving an array of paths.
- **[Feature]** Add `patchman.ref.Var.{replace, update}` patches for modifying static variables.
  - Updating a variable then replacing it is not supported yet.
- **[Feature]** Add `Ref.call` macro to statically dispatch `hf.*` methods.
- **[Feature]** Allow exporting a field (*via* `@:diExport`) with a different type.
- **[Fix]** Fix `Obfu.raw` not properly obfuscating the empty string.

# 0.9.2 (2020-09-04)

- **[Fix]** Fix obfuscation protection conditional compilation.

# 0.9.1 (2020-09-03)

- **[Fix]** Add obfuscation protection support for non-Flash targets.

# 0.9.0 (2020-09-02)

- **[Breaking Change]** Add `patchman.ref.{FunctionVoid, MethodVoid}` types; remove `afterVoid` method.
- **[Breaking Change]** Move `patchman.{Symbol, ds.*}` types to `ef.{Symbol, ds.*}`; keep old names as typedefs.
- **[Feature]** Add `PrefixPatch` and `PostfixPatch`, as well as the associated methods on `patchman.ref.*` types.
- **[Feature]** Add `Nil.unwrap` method.
- **[Feature]** Add `Obfu.field`, `Obfu.setField`, `Obfu.hasField` macros.
- **[Feature]** Implement the `game-events`, `run` and `run-start` host modules.
- **[Fix]** Deprecate `patchman.{Symbol, ds.*}` typedefs, use `ef.{Symbol, ds.*}` types instead.
- **[Fix]** Update game types to `0.5.2`.
- **[Fix]** Update dependencies.
- **[Fix]** Refactor the `console` and `env` host modules.
- **[Internal]** Use codegen to generate `ref.MethodN` and `ref.FunctionN` types.
- **[Internal]** Refactor `patchman.fpatch.HandlerStore`.
- **[Internal]** Update module format from CommonJS to ESM

# 0.8.2 (2020-04-05)

- **[Feature]** Add `patchman --check` to check Haxe code without creating a SWF file.
- **[Fix]** Add support for custom messages in `Assert.debug`.

# 0.8.1 (2020-02-18)

- **[Feature]** Add the `_class` method to the `hf.classes.*` classes.
- **[Feature]** Support the subclassing of `hf.*` classes.
- **[Feature]** Add `module.Data` for the loader's "data" module.
- **[Fix]** Deprecate `Ref.get` for classes, use the `_class` method instead.
- **[Fix]** Update game types to `0.4.1`.
- **[Fix]** Make CLI executable after build.

# 0.8.0 (2020-01-08)

- **[Breaking Change]** Remove deprecated `Ref` macros (`Ref.call`, `Ref.instanciate`, and `Ref.get` for static fields).
- **[Breaking Change]** Remove deprecated typedef `patchman.Hf`.
- **[Breaking Change]** Remove deprecated typedefs `patchman.ds.{FrozenStringMap, FrozenStringSet, StringSet}`.
- **[Breaking Change]** Update game types to `0.4.0`.
- **[Fix]** Enable strict flash checks.

# 0.7.2 (2020-01-07)

- **[Feature]** Improve the type-safety of the `hf` typings by adding a preprocessing step.
  - Static fields are moved to dedicated `hf.classes.*` classes
  - A `_new` method is added to the `hf.classes.*` classes to instantiate them
- **[Fix]** Deprecate `Ref.get` for static fields and `Ref.call`, use direct field access instead.
- **[Fix]** Deprecate `Ref.instanciate`, use the `_new` method instead.
- **[Fix]** Update dependencies.

# 0.7.1 (2019-12-10)

- **[Fix]** Update dependencies.
- **[Fix]** Move obfuscation to `@eternalfest/obf`.

# 0.7.0 (2019-12-10)

- **[Breaking Change]** Add the `Patchman.builder()` API to specify initial module values.
- **[Breaking Change]** Use `FrozenArray<T>` instead of `Array<T>` for `@:diExport`.
- **[Breaking Change]** Remove `IPatchProvider` (use `@:diExport` instead).
- **[Feature]** Support references to functions with optional arguments in `Ref` macros.
- **[Feature]** Display an error when using `Ref` macros on classes annotated with `@:interface`.
- **[Fix]** Move `hf` types and obfuscation map to package `@eternalfest-types/game`.
- **[Fix]** Deprecate `patchman.Hf`, use `hf.Hf` instead.
- **[Fix]** Reobfuscate `insert`.
- **[Fix]** Fix class names for method references.
- **[Fix]** Fix missing context type parameter in `ref.Method4`.
- **[Internal]** Add codegen step in build process.
- **[Internal]** Update Docker image used by CI/CD.
- **[Internal]** Update build tools.

# 0.6.4 (2019-11-13)

- **[Fix]** Don't obfuscate `insert`, to fix an issue with `Array.insert`.

# 0.6.3 (2019-11-04)

- **[Fix]** Fix type definitions for "Le supplice des téméraire".

# 0.6.2 (2019-10-29)

- **[Feature]** Add support for initial module values.

# 0.6.1 (2019-10-28)

- **[Feature]** Add `Ref` support for functions and methods with up to 9 arguments.

# 0.6.0 (2019-10-23)

- **[Breaking Change]** Refactor `patchman.ds` module:
  - Rename `WeakSet.has` to `exists`;
  - Rework `Set` to accept more key types;
  - Add generic `FrozenSet` and `FrozenMap` abstracts;
  - Add methods `keys` and `get` to `ReadOnlyMap`;
  - Remove `ISet` and `HashSet`;
  - `StringSet`, `FrozenStringSet` and `FrozenStringMap` are now typedefs.
- **[Feature]** Add `FrozenArray.{concat, copy, filter, map, slice}` methods.
- **[Feature]** Add `patchman.ds.{Set, FrozenSet, FrozenArray, FrozenMap}.of`.
- **[Feature]** Add `patchman.ds.Nil`.
- **[Feature]** Add `patchman.ds.Memoizer`.
- **[Feature]** Add `Ref.instanciate` to call `hf` constructors.
- **[Internal]** Move macro implementations to dedicated `patchman.macro.*Impl` classes.

# 0.5.0 (2019-09-19)

- **[Breaking change]** Rename `of` to `from` in frozen data structures.

# 0.4.1 (2019-09-19)

- **[Feature]** `Array` dependencies now also look into module fields marked with `@:diExport`.
- **[Feature]** Add `patchman.ds.ReadOnlyMap` and `patchman.ds.FrozenStringMap`.

# 0.4.0 (2019-09-16)

- **[Breaking change]** Use `__dollar__` instead of `_S_` in identifiers for `$`.
- **[Breaking change]** Move `patchman.flash` to `ef`.
- **[Fix]** Move super binding table initialization inside a function.
- **[Fix]** Use collapsed representation for `SuperBindingTable` values.

# 0.3.0 (2019-09-16)

- **[Breaking change]** Use `@:diFactory` metadata to mark dependency injection constructor.
- **[Breaking change]** Use `__diRecord` for dependency injection record (instead of `__pmDeps` and `__pmFactory`).
- **[Feature]** Add more data structures in `patchman.ds`: sets, read-only variants, frozen variants.
- **[Feature]** Add `ef.flash` to expose Flash classes compatible with other targets.
- **[Internal]** Refactor Dependency Injection system.

# 0.2.3 (2019-09-12)

- **[Fix]** Allow to patch inherited methods.

# 0.2.2 (2019-09-07)

- **[Fix]** Avoid `Symbol` reliance on the order of static initialization.

# 0.2.1 (2019-09-06)

- **[Fix]** Fix `Env` factory function.

# 0.2.0 (2019-09-06)

- **[Breaking change]** Remove `utils.ColorMatrix` and `Utils` class.
- **[Breaking change]** Move `WeakMap` to `patchman.ds`.
- **[Breaking change]** Move `Console` and `Env` to `patchman.module`.
- **[Breaking change]** Return `IPatch`s instead of concrete classes from `ref.*` classes.
- **[Feature]** Add `OncePatch` class to help implementing idempotent patches.
- **[Feature]** Add `WeakSet` class.
- **[Feature]** Add `Assert.debug` function.
- **[Feature]** Add support for custom factory functions (`__pmFactory`).
- **[Feature]** Add support for array dependencies.
- **[Internal]** Refactor module loader.

# 0.1.2 (2019-09-04)

- **[Fix]** Set `patchman.haxe` metadata in `package.json`.

# 0.1.1 (2019-09-04)

- **[Feature]** Add `patchman --idea` flag to emit Intellij IDEA config.
- **[Fix]** Fix deep dependency resolution.

# 0.1.0 (2019-09-02)

- **[Feature]** First release
