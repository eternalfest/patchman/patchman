package patchman.macro;

class Memoizer<K, V> {

  private var cache: Map<String, V>;
  private var hashFn: K -> String;
  private var computeFn: K -> V;

  public function new(hash: K -> String, compute: K -> V) {
    this.cache = new Map();
    this.hashFn = hash;
    this.computeFn = compute;
  }

  public function get(key: K): Null<V> {
    return this.cache.get(this.hashFn(key));
  }

  public function exists(key: K): Bool {
    return this.cache.exists(this.hashFn(key));
  }

  public function iterator(): Iterator<V> {
    return this.cache.iterator();
  }

  public function compute(key: K): V {
    var hashed: String = this.hashFn(key);
    if (this.cache.exists(hashed)) {
      return this.cache.get(hashed);
    }

    var val: V = this.computeFn(key);
    this.cache.set(hashed, val);
    return val;
  }

  public function clear(): Void {
    this.cache = new Map();
  }
}
