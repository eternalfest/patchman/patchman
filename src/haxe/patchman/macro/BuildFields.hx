package patchman.macro;

#if macro
import haxe.ds.StringMap;
import haxe.macro.Expr.Field;
import haxe.macro.Expr.MetadataEntry;
import etwin.ds.ReadOnlyArray;

class BuildFields {
  private var fieldsByName(default, null): StringMap<Field>;
  private var fieldsByMeta(default, null): StringMap<Array<FieldAndMeta>>;

  public function new(fields: ReadOnlyArray<Field>) {
    var fieldsByName: StringMap<Field> = new StringMap();
    var fieldsByMeta: StringMap<Array<FieldAndMeta>> = new StringMap();
    for (field in fields) {
      Assert.debug(!fieldsByName.exists(field.name));
      fieldsByName.set(field.name, field);
      if (field.meta != null) {
        for (meta in field.meta) {
          var entries: Null<Array<FieldAndMeta>> = fieldsByMeta.get(meta.name);
          if (entries == null) {
            entries = ([]: Array<FieldAndMeta>);
            fieldsByMeta.set(meta.name, entries);
          }
          entries.push({field: field, meta: meta});
        }
      }
    }

    this.fieldsByName = fieldsByName;
    this.fieldsByMeta = fieldsByMeta;
  }

  public function getByMeta(meta: String): ReadOnlyArray<FieldAndMeta> {
    var result: Null<Array<FieldAndMeta>> = this.fieldsByMeta.get(meta);
    return result != null ? result : [];
  }

  public function getByName(name: String): Null<Field> {
    return this.fieldsByName.get(name);
  }
}

typedef FieldAndMeta = {
  field: Field,
  meta: MetadataEntry,
}

#end
