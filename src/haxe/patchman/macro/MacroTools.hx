package patchman.macro;

#if macro
import haxe.macro.ComplexTypeTools;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.TypeTools;

class MacroTools {

  private static var MEMOIZERS: Array<Memoizer<Void, Dynamic>> = [];

  private static dynamic function installReuseHandler(): Void {
    Context.onMacroContextReused(function() {
      for (memo in MEMOIZERS) {
        memo.clear();
      }
      return true;
    });

    installReuseHandler = function() {};
  }

  // Make a memoizer which will be automatically reset when the macro context changes.
  public static function memoizer<K, V>(hash: K -> String, compute: K -> V): Memoizer<K, V> {
    var memo: Memoizer<K, V> = new Memoizer(hash, compute);
    MEMOIZERS.push(cast memo);
    installReuseHandler();
    return memo;
  }

  // Catch any macro errors thrown during `fn` and relocate them to `pos`
  // if their position is `Context.currentPos()`.
  public static function relocateErrors<T>(pos: Position, fn: Void -> T): T {
    try {
      return fn();

    } catch (e: haxe.macro.Error) {
      var cpos: { min: Int, max: Int, file: String } = Context.getPosInfos(Context.currentPos());
      var epos: { min: Int, max: Int, file: String } = Context.getPosInfos(e.pos);

      if (cpos.min == epos.min && cpos.max == epos.max && cpos.file == epos.file) {
         e.pos = pos;
      }
      throw e;
    }
  }

  public static function addFieldMetadata(field: Field, name: String, ?params: Array<Expr>): Void {
    var meta = { name: name, params: params, pos: field.pos };
    if (field.meta == null) {
      field.meta = [meta];
    } else {
      field.meta.push(meta);
    }
  }

  public static function pathToTypePath(path: Array<String>, ?params: Array<TypeParam>): TypePath {
    return {
      pack: path.slice(0, -1),
      name: path[path.length - 1],
      sub: null,
      params: params == null ? [] : params,
    };
  }

  public static function typePathToPath(typePath: TypePath): Array<String> {
    var path = typePath.pack.copy();
    path.push(typePath.name);
    if (typePath.sub != null)
      path.push(typePath.sub);

    return path;
  }

  public static function pathToType(path: Array<String>): Type {
    var tPath: TypePath = MacroTools.pathToTypePath(path);
    var cType: ComplexType = ComplexType.TPath(tPath);
    return ComplexTypeTools.toType(cType);
  }

  public static function pathToClass(path: Array<String>): ClassType {
    var type = MacroTools.pathToType(path);
    return TypeTools.getClass(type);
  }

  public static function isStdType(type: ComplexType, name: String): Bool {
    switch (type) {
      case ComplexType.TPath(p):
        return p.pack.length == 0 && p.name == "StdTypes" && p.sub == name;
      default:
        return false;
    }
  }

  // Gets the parts from a "path expression": a sequence of field expressions.
  public static function getExprPath(e: Expr): Null<Array<String>> {
    var result = [];
    var cur: Expr = e;
    while (cur != null) {
      cur = switch (cur.expr) {
        case ExprDef.EConst(Constant.CIdent(s)):
          result.push(s);
          null;
        case ExprDef.EField(obj, field):
          result.push(field);
          obj;
        default:
          // Invalid path
          return null;
      }
    }
    result.reverse();
    return result.length == 0 ? null : result;
  }

  // Make an expression accessing the given path from the given expression.
  public static function pathToExpr(expr: Expr, fields: Array<String>, ?pos: Position): Expr {
    if (pos == null)
      pos = Context.currentPos();
    for (field in fields) {
      expr = macro @:pos(pos) ${expr}.$field;
    }
    return expr;
  }
}
#end
