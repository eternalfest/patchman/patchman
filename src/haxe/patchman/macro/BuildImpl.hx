package patchman.macro;

#if macro
import haxe.ds.StringMap;
import haxe.macro.ComplexTypeTools;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.TypeTools;
import patchman.di.ModuleRecord;
import patchman.di.Dependency;
import patchman.di.DotPath;
import patchman.di.Factory;
import etwin.ds.ReadOnlyArray;
import etwin.ds.Set;
import patchman.macro.BuildFields;
import patchman.macro.MacroTools;

class BuildImpl {

  public static function di(): Array<Field> {
    var classRef: Null<Ref<ClassType>> = Context.getLocalClass();
    if (classRef == null) {
      Context.error("`Build.di` failed to resolve local class", Context.currentPos());
    }
    if (classRef.get().params.length > 0) {
      Context.error("`Build.di` does not support parametrized classes", Context.currentPos());
    }

    var fields: Array<Field> = Context.getBuildFields();

    var recordExpr = getModuleRecordExpr(classRef, new BuildFields(fields));

    // append the `__diRecord` field
    var diRecord: Field = {
      name: patchman.di.ModuleLoader.SYMBOL_DI_RECORD,
      access: [Access.APublic, Access.AStatic],
      kind: FieldType.FVar(macro: patchman.di.ModuleRecord, recordExpr),
      pos: Context.currentPos(),
    };
    protectFromDCE(diRecord);
    fields.push(diRecord);
    return fields;
  }

  private static function protectFromDCE(field: Field): Void {
    // Only protect if DCE is enabled, to not pollute the documentation with
    // the generated `:ifFeature` metadata.
    if (Context.definedValue("dce") == "full") {
      MacroTools.addFieldMetadata(field, ":ifFeature", [macro "patchman.di.ModuleLoader.load"]);
    }
  }

  private static function getModuleRecordExpr(classRef: Ref<ClassType>, fields: BuildFields): ExprOf<ModuleRecord> {
    var factory: FieldAndFactory = getFactory(fields);
    var dependencies: Array<Dependency> = getDependenciesFromFactory(factory.field, classRef);
    var interfaces: Array<DotPath> = getInterfaces(classRef);
    var exports: ModuleExports = getExports(fields);
    var shareDependencies: Bool = true; // TODO: Do not default to `true`?

    var depExpr = arrayToFrozenExpr(dependencies, macro etwin.ds.FrozenArray, dependencyToExpr);
    var interfExpr = arrayToFrozenExpr(interfaces, macro etwin.ds.FrozenSet, dotPathToExpr);

    var classExpr: Expr = classToExpr(classRef);
    return macro {
      path: Type.getClassName($e{classExpr}),
      factory: $e{factoryToExpr(factory.factory, classExpr)},
      dependencies: $e{depExpr},
      interfaces: $e{interfExpr},
      exports: $e{exportsToExpr(exports.exports)},
      iterableExports: $e{exportsToExpr(exports.iterableExports)},
      shareDependencies: $v{shareDependencies},
    };
  }

  private static function getInterfaces(classRef: Ref<ClassType>): Array<DotPath> {
    var interfaces: Array<DotPath> = new Array();
    for (ifaceInfo in classRef.get().interfaces) {
      var iface: ClassType = ifaceInfo.t.get();
      if (iface.params.length > 0) {
        Context.error("`Build.di` does not support classes implementing parametrized interfaces", Context.currentPos());
      }
      // TODO: Check `iface.Module`
      var absPath: String = iface.pack.join(".") + "." + iface.name;
      interfaces.push(absPath);
    }
    return interfaces;
  }

  private static function getFactory(fields: BuildFields): FieldAndFactory {
    var factoryFields: ReadOnlyArray<FieldAndMeta> = fields.getByMeta(":diFactory");
    var result = switch (factoryFields.length) {
      case 0: {
        var factoryField: Null<Field> = fields.getByName("new");
        if (factoryField == null) {
          Context.error("Failed to find module factory: no constructor and no `@:diFactory` function", Context.currentPos());
        }
        ({field: factoryField, factory: Factory.Constructor(null)});
      }
      case 1: {
        var factoryField: Field = factoryFields[0].field;
        var meta: MetadataEntry = factoryFields[0].meta;
        if (meta.params != null && meta.params.length > 0) {
          Context.error("`@:diFactory` does not accept arguments", meta.pos);
        }
        ({field: factoryField, factory: Factory.Function(null, factoryField.name)});
      }
      default: {
        Context.error("Duplicate `@:diFactory` function", factoryFields[1].field.pos);
      }
    };

    protectFromDCE(result.field);
    return result;
  }

  private static function getDependenciesFromFactory(factory: Field, classRef: Ref<ClassType>): Array<Dependency> {
    var isConstructor: Bool = factory.name == "new";
    var isValidMethod: Bool = isConstructor || factory.access.indexOf(Access.AStatic) >= 0;

    var factoryFn: Function = switch (factory.kind) {
      case FieldType.FFun(fn) if (isValidMethod): fn;
      case _: Context.error("The module factory must be the constructor or a static method", factory.pos);
    };

    if (!isConstructor) {
      var retType: Null<Type> = ComplexTypeTools.toType(factoryFn.ret);
      if (retType == null) {
        Context.error("Missing return type in module factory", factory.pos);
      }

      var classType = Type.TInst(classRef, []);
      // TODO: relax the type constraint when we know how to handle subclasses
      if (!(Context.unify(retType, classType) && Context.unify(classType, retType))) {
        Context.error('Module factory must have return type ${TypeTools.toString(classType)}', factory.pos);
      }
    }

    var dependencies: Array<Dependency> = [];
    for (arg in factoryFn.args) {
      var argType: Null<ComplexType> = arg.type;
      if (argType == null) {
        Context.error("Missing argument type in module factory", factory.pos);
      }
      // The naive way to get an instance of the `Array<T>` type would be to do:
      // `Context.getType("Array")`, but because the path doesn't have any packages, this
      // will cause the compiler to continue typing the current package (to make sure there
      // is no `Array` class defined inside). If the current package has other classes
      // annotated with `Build.di()`, the compiler will call the macro recursively, causing
      // an extremely deep call stack and eventually a stack overflow.
      //
      // Instead, we use an array literal to obtain the type without triggering name lookups.
      var arrayType = Context.typeof(macro []);
      dependencies.push(typeToDependency(argType, arrayType, null, factory.pos));
    }

    return dependencies;
  }

  private static function getExports(fields: BuildFields): ModuleExports {
    var exportFields: ReadOnlyArray<FieldAndMeta> = fields.getByMeta(":diExport");

    var simpleExports: Map<DotPath, Array<String>> = new Map();
    var iterableExports: Map<DotPath, Array<String>> = new Map();
    var seen: Set<String> = new Set();

    for (export in exportFields) {
      var name: String = export.field.name;
      var params: Array<Expr> = export.meta.params;

      var isNonStatic = export.field.access.indexOf(Access.AStatic) < 0;
      var fieldType: Null<ComplexType> = switch (export.field.kind) {
        case FieldType.FProp("default", "null" | "never", t, _) if (isNonStatic): t;
        case _: Context.error("`@:diExport` can only be used on non-static read-only fields", export.field.pos);
      };

      if (fieldType == null) {
        Context.error("Missing type in `@:diExport` field", export.field.pos);
      }

      var exportTypes: Array<Null<Type>> = if (params == null || params.length == 0) {
        [null]; // Use the type of the field as the export type.
      } else {
        params.map(function(expr) {
          var path = MacroTools.getExprPath(expr);
          if (path == null) {
            Context.error('`@:diExport`: invalid type path', expr.pos);
          }
          return MacroTools.pathToType(path);
        });
      };

      for (exportType in exportTypes) {
        var arrayType = Context.getType("etwin.ds.FrozenArray");
        switch typeToDependency(fieldType, arrayType, exportType, export.field.pos) {
          case Dependency.Simple(classPath):
            if (!simpleExports.exists(classPath)) {
              simpleExports.set(classPath, []);
            }
            simpleExports.get(classPath).push(name);
          case Dependency.Array(classPath):
            if (!iterableExports.exists(classPath)) {
              iterableExports.set(classPath, []);
            }
            iterableExports.get(classPath).push(name);
        }
      }

      protectFromDCE(export.field);
    }

    return {
      exports: simpleExports,
      iterableExports: iterableExports,
    };
  }

  private static function typeToDependency(ty: ComplexType, arrayType: Type, exportTy: Null<Type>, pos: Position): Dependency {
    return MacroTools.relocateErrors(pos, function() {
      var type: Type = ComplexTypeTools.toType(ty);

      var isArrayDep: Bool = if (Context.unify(type, arrayType)) {
        type = switch (arrayType) {
          case Type.TInst(_, [mono = Type.TMono(_)]): mono;
          case Type.TAbstract(_, [mono = Type.TMono(_)]): mono;
          case _: Context.error("InternalError: couldn't extract monomorph", pos);
        };
        true;
      } else {
        false;
      };

      if (exportTy != null) {
        if (!Context.unify(type, exportTy)) {
          Context.error('`Build.di()`: can\'t assign ${TypeTools.toString(type)} to ${TypeTools.toString(exportTy)}', pos);
        }
        type = exportTy;
      }

      var path = switch (Context.follow(type)) {
        case t = Type.TInst(_, []): TypeTools.toString(t);
        case Type.TInst(_, _): Context.error("`Build.di()` does not support parametrized types here", pos);
        case _: Context.error("`Build.di()` only supports class types here", pos);
      };

      return isArrayDep ? Dependency.Array(path) : Dependency.Simple(path);
    });
  }

  private static function exportsToExpr(value: Map<DotPath, Array<String>>): Expr {
    return stringMapToExpr(
      value,
      dotPathToExpr,
      arrayToFrozenExpr.bind(
        _,
        macro etwin.ds.FrozenSet,
        function(field: String) return macro $v{field}
      )
    );
  }

  // TODO: remove this once `etwin.ds.FrozenSet/Map/Array.of()` is fixed for empty collections.
  private static function arrayToFrozenExpr<V>(array: Array<V>, frozenType: Expr, reifyElem: V -> Expr): Expr {
    if (array.length == 0) {
      return macro $e{frozenType}.empty();
    } else {
      var elems = array.map(reifyElem);
      return macro $e{frozenType}.of($a{elems});
    }
  }

  private static function stringMapToExpr<V>(map: StringMap<V>, reifyKey: String -> Expr, reifyValue: V -> Expr): Expr {
    var entries: Array<Expr> = [];
    for (key in map.keys()) {
      var value: V = map.get(key);
      var keyExpr: Expr = reifyKey(key);
      var valueExpr: Expr = reifyValue(value);
      entries.push({
        expr: ExprDef.EBinop(Binop.OpArrow, keyExpr, valueExpr),
        pos: Context.currentPos()
      });
    }

    if (entries.length == 0) {
      return macro etwin.ds.FrozenMap.empty();
    } else {
      return macro etwin.ds.FrozenMap.of($a{entries});
    }
  }

  private static function dotPathToExpr(value: DotPath): Expr {
    var parts: Array<String> = value.split(".");
    return macro (($v{parts}).join("."));
  }

  private static function dependencyToExpr(value: Dependency): Expr {
    return switch (value) {
      case Dependency.Simple(depPath): macro patchman.di.Dependency.Simple($e{dotPathToExpr(depPath)});
      case Dependency.Array(depPath): macro patchman.di.Dependency.Array($e{dotPathToExpr(depPath)});
    }
  }

  private static function factoryToExpr(value: Factory, classExpr: Expr): Expr {
    return switch (value) {
      case Factory.Constructor(_): macro patchman.di.Factory.Constructor($e{classExpr});
      case Factory.Function(_, fName): macro patchman.di.Factory.Function($e{classExpr}, $v{fName});
      case Factory.Value(_): throw "Can't convert Factory.Value to an expression";
    };
  }

  private static function classToExpr(classRef: Ref<ClassType>): Expr {
    var cType = TypeTools.toComplexType(TInst(classRef, []));
    switch (cType) {
      case TPath(path): {
        var e: Null<Expr> = null;
        for (part in MacroTools.typePathToPath(path)) {
          e = e == null ? (macro $i{part}) : (macro ${e}.$part);
        }
        return e;
      }
      default: throw "Expected type path for class type";
    }
  }
}

private typedef FieldAndFactory = {
  field: Field,
  factory: Factory,
}

private typedef ModuleExports = {
  exports: Map<DotPath, Array<String>>,
  iterableExports: Map<DotPath, Array<String>>,
}
#end
