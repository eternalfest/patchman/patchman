package patchman.macro;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

import patchman.macro.MacroTools;

using haxe.macro.ExprTools;
using haxe.macro.TypeTools;

class HfClassImpl {

  public static function hfClass(): Null<Array<Field>> {
    var cls = Context.getLocalClass().get();
    if (isClassInHfPack(cls) || cls.superClass == null)
      return null;

    var superClass = cls.superClass.t.get();
    var superClassPath = superClass.pack.concat([superClass.name]);

    if (!cls.meta.has(":hfTemplate")) {
      Context.error("Classes inheriting from `hf.*` classes must be marked with `:hfTemplate`", cls.pos);
    }

    // Rewrite constructor
    var fields: Array<Field> = Context.getBuildFields().map(function(field) {
      switch (field) {
        case { name: "new", kind: FFun(constructor) }:
          // If the class is an Entity, the constructor will be implicitly instantiated
          // thanks to Object.registerClass, so make sure it isn't removed by DCE.
          // (the @:keep on the super-class' constructor isn't enough)
          MacroTools.addFieldMetadata(field, ":keep");
          return {
            access: field.access,
            doc: field.doc,
            kind: FFun(rewriteConstructor([cls.name], isClassInHfPack(superClass) ? superClassPath : null, constructor)),
            meta: field.meta,
            name: field.name,
            pos: field.pos,
          };
        default:
          return field;
      }
    });

    // Add metadata field
    fields.push({
      name: patchman.flash.Magic.SYMBOL_TEMPLATE_CLASS_MAP,
      access: [Access.APrivate, Access.AStatic],
      kind: FieldType.FVar(
        macro: etwin.ds.WeakMap<Dynamic, Dynamic>,
        macro new etwin.ds.WeakMap()
      ),
      pos: cls.pos,
      // This field will be accessed dynamically; exclude it from DCE
      meta: [{ name: ":keep", pos: Context.currentPos() }]
    });

    // Add 'getClass(hf: hf.Hf): Class<Self>' method
    fields.push({
      name: "getClass",
      access: [Access.APublic, Access.AStatic, Access.AInline],
      kind: FieldType.FFun({
        args: [{ name: "hf", type: macro: hf.Hf }],
        ret: ComplexType.TPath(MacroTools.pathToTypePath(["Class"], [TPType(TInst(Context.getLocalClass(), []).toComplexType())])),
        expr: macro {
          return patchman.flash.Magic.getOrCreateClassFromTemplate(
            $i{ cls.name }, 
            $e{ isClassInHfPack(superClass) ? macro $p{ superClassPath }._class() : macro $p{ superClassPath }.getClass(hf) }
          );
        },
      }),
      pos: cls.pos,
    });

    return fields;
  }

  private static function isClassInHfPack(cls: Null<ClassType>): Bool {
    return cls != null && cls.pack.length != 0 && cls.pack[0] == "hf";
  }

  private static function rewriteConstructor(cls: Array<String>, hfSuperClass: Null<Array<String>>, constructor: Function): Function {
    if (constructor.expr == null)
      return constructor;

    function rewriteSuperCalls(e: Expr): Expr {
      var args = switch (e.expr) {
        case ECall({ expr: EConst(CIdent("super")) }, args): args;
        default: return e.map(rewriteSuperCalls);
      };
      args = args.map(function(e) return e.map(rewriteSuperCalls));

      // Inheriting from another templated class
      if (hfSuperClass == null) {
        return macro {
          // Call 'super' constructor
          $e{e};
          // Get the superclass object (it was set by the constructor call)
          var __superClass = Reflect.field(Reflect.field(this, "__proto__"), "constructor");
          // Replace the 'super' prototype by a generated class
          var __realClass = patchman.flash.Magic.getOrCreateClassFromTemplate($p{ cls }, __superClass);
          Reflect.setField(this, "__proto__", Reflect.field(__realClass, "prototype"));

          // To force the block to be typed Void
          if (false) {}
        }
      // Directly inheriting from an hf.* class
      } else {
        // If the constructor call has no arguments, typecheck will fail, so we can use dummy values here
        var hfArg = args.length > 0 ? args[0] : macro untyped null;
        var realArgs = args.length > 0 ? args.slice(1) : [];

        return macro {
          if (false) {
            // This will never execute, but is here to:
            // - validate that the arguments are typed correctly
            // - convince the compiler we've done the 'super' call
            super($a{args});
          } else {
            var __selfClass = $p{ cls };
            var __superClass: Dynamic;

            // Replace the prototype by a generated class, only if it wasn't already replaced.
            // Also retrieve the actual superclass of this object.
            if (untyped __instanceof__(this, __selfClass)) {
              __superClass = $e{ MacroTools.pathToExpr(hfArg, hfSuperClass.slice(1)) }._class();
              if (__superClass != null) {
                var __realClass = patchman.flash.Magic.getOrCreateClassFromTemplate(__selfClass, __superClass);
                Reflect.setField(this, "__proto__", Reflect.field(__realClass, "prototype"));
              }
            } else {
              __superClass = Reflect.field(Reflect.field(Reflect.field(this, "__proto__"), "__proto__"), "constructor");
            }

            if (__superClass == null) {
              throw new etwin.Error($v{ 'Couldn\'t find real class: ${hfSuperClass.join(".")}' });
            }

            // Call constructor of the actual superclass
            Reflect.callMethod(this, __superClass, $a{ realArgs });
          }
        };
      }      
    }

    return {
      args: constructor.args,
      expr: rewriteSuperCalls(constructor.expr),
      params: constructor.params,
      ret: constructor.ret,
    };
  }
}

#end
