package patchman.macro;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.TypeTools;
import patchman.macro.Memoizer;
import patchman.macro.MacroTools;
import patchman.Ref;

class RefImpl {

  public static function call(hf: Expr, ref: Expr, obj: Expr, args: Array<Expr>): Expr {
    var target: ResolvedPath = resolvePathFromExpr(ref, "Ref.call", obj);
    var refPath: Array<String> = getFieldsFrom(target);
    var pos = Context.currentPos();

    if (target.field == null) {
      Context.error("`Ref.call`: Cannot call class" + refPath.join("."), ref.pos);
    }

    var type = eraseTypeParameters(target.field.type, target.field.params, ref.pos);

    var fnTypes: ComplexFunctionType = switch (target.field.kind) {
      case FieldKind.FVar(_, _):
        Context.error("`Ref.call`: Cannot call non-method: " + refPath.join("."), ref.pos);
      case FieldKind.FMethod(MethNormal | MethDynamic):
        if (target.isStatic)
          Context.error("`Ref.call`: Cannot call static method: " + refPath.join("."), ref.pos);
        functionTypeToComplexType(type, refPath, ref.pos);
      case FieldKind.FMethod(_):
        Context.error("`Ref.call`: Cannot call inline or macro method: " + refPath.join("."), ref.pos);
    };

    hf = checkType(hf, macro: hf.Hf);
    obj = checkType(obj, ComplexType.TPath(MacroTools.pathToTypePath(target.classPath)));

    var nbParams = fnTypes.params.length;
    if (args.length != nbParams) {
      Context.error('`Ref.call`: wrong number of arguments (expected $nbParams, got ${args.length})', pos);
    }
    args = [for (i in 0...args.length) checkType(args[i], fnTypes.params[i])];

    refPath.pop(); // Remove field name and get class path.
    var cls =  MacroTools.pathToExpr(hf, refPath, ref.pos);

    var ret = macro @:pos(pos) Reflect.callMethod(
      $obj,
      Reflect.field(Reflect.field($cls, "prototype"), $v{target.field.name}),
      $a{args}
    );

    if (MacroTools.isStdType(fnTypes.ret, "Void")) {
      // Work-around for Void types, because we can't cast Dynamic to Void.
      return macro @:pos(pos) (function() $ret)();
    } else {
      return checkType(ret, fnTypes.ret);
    }
  }

  public static function auto(e: Expr): Expr {
    var target: ResolvedPath = resolvePathFromExpr(e, "Ref.auto");

    var refPath: Array<String> = getFieldsFrom(target);
    var typeParams: Array<TypeParam> = [];
    var constructorName: Null<String> = null;

    if (target.field == null) {
      typeParams.push(TypeParam.TPType(ComplexType.TPath(MacroTools.pathToTypePath(target.classPath))));
      constructorName = "Class";
    } else {
      if (!target.isStatic) {
        typeParams.push(TypeParam.TPType(ComplexType.TPath(MacroTools.pathToTypePath(target.classPath))));
      }

      var type = eraseTypeParameters(target.field.type, target.field.params, e.pos);

      switch (target.field.kind) {
        case FieldKind.FVar(_, _):
          if (!target.isStatic) {
            Context.error("Cannot reference non-static var field: " + refPath.join("."), e.pos);
          }
          constructorName = "Var";
          var varType: Null<ComplexType> = TypeTools.toComplexType(type);
          if (varType == null) {
            Context.error("Failed to resolve type for var " + refPath.join("."), e.pos);
          }
          typeParams.push(TypeParam.TPType(varType));

        case FieldKind.FMethod(MethNormal | MethDynamic):
          var fnTypes: ComplexFunctionType = functionTypeToComplexType(type, refPath, e.pos);
          var nbParams: Int = fnTypes.params.length;
          var hasVoidResult: Bool = MacroTools.isStdType(fnTypes.ret, "Void");

          if (nbParams > 9) {
            Context.error('Unsupported number of arguments ($nbParams): ${refPath.join(".")}', e.pos);
          }
          constructorName = (target.isStatic ? "Function" : "Method") + (hasVoidResult ? "Void" : "") + nbParams;

          for (param in fnTypes.params) {
            typeParams.push(TypeParam.TPType(param));
          }
          if (!hasVoidResult) {
            typeParams.push(TypeParam.TPType(fnTypes.ret));
          }

        case FieldKind.FMethod(_):
          Context.error("Cannot reference inline or macro method: " + refPath.join("."), e.pos);
      }
    }

    if (constructorName == null) {
      Context.error("Failed to resolve `Ref` constructor", e.pos);
    }

    var result: Expr = {
      expr: ExprDef.ENew(
        {
          pack: ["patchman", "ref"],
          name: constructorName,
          sub: null,
          params: typeParams
        },
        [macro $v{refPath}]
      ),
      pos: e.pos,
    }

    return result;
  }

  private static function resolvePathFromExpr(e: Expr, macroName: String, ?selfExpr: Expr): ResolvedPath {
    var p: Null<Array<String>> = MacroTools.getExprPath(e);
    if (p == null) {
      Context.error('Invalid expression passed to `${macroName}`, expected path of identifiers', e.pos);
    }

    var resolved = resolvePath(p, e.pos, selfExpr);
    if (resolved.classPath.length == 0 || resolved.classPath[0] != "hf") {
      Context.error('`${macroName}` only supports items from the `hf` package', e.pos);
    }
    if (resolved.classType.meta.has(":interface")) {
      Context.error('`${macroName}`: This class doesn\'t exist at runtime and can\'t be referenced', e.pos);
    }
    return resolved;
  }

  private static var CLASSES_INFO: Memoizer<ClassType, ClassInfo> = MacroTools.memoizer(
    function(cls: ClassType) { return cls.pack.join("#") + "#" + cls.name; },
    function(cls: ClassType) {
      var fields: Map<String, ClassField> = new Map();
      var statics: Map<String, ClassField> = new Map();

      try {
        var staticCls: Array<String> = cls.pack.concat([cls.name]);
        staticCls.insert(1, "classes");

        for (field in MacroTools.pathToClass(staticCls).fields.get()) {
          statics.set(field.name, field);
        }
      } catch (e: Dynamic) {
        // No associated `hf.classes.*` type, ignore error
      }

      var curCls: Null<ClassType> = cls;
      while (curCls != null) {
        for (field in curCls.fields.get()) {
          if (!fields.exists(field.name)) {
            fields.set(field.name, field);
          }
        }
        // TODO handle parametrized superclasses ?
        curCls = curCls.superClass == null ? null : curCls.superClass.t.get();
      }

      return {
        cls: cls,
        path: cls.pack.concat([cls.name]),
        fields: fields,
        statics: statics,
      };
    }
  );

  private static function resolvePath(p: Array<String>, pos: Position, selfExpr: Null<Expr>): ResolvedPath {
    inline function error() {
      Context.error("Failed to resolve path: " + p.join("."), pos);
    }

    if (p.length == 0) {
      error();
    }

    var cls: Null<ClassType> = null;
    var fieldName: Null<String> = null;

    // Handle special cases `this`, `super`, `this.foo`, `super.foo`.
    if (selfExpr != null && p.length <= 2) {
      if (p[0] == "this") {
        cls = TypeTools.getClass(Context.typeof(selfExpr));
      } else if (p[0] == "super") {
        cls = TypeTools.getClass(Context.typeof(selfExpr));
        if (cls.superClass == null) {
          error();
        }
        // TODO handle parametrized superclasses ?
        cls = cls.superClass.t.get();
      }
      fieldName = p.length == 1 ? null : p[1];
    }

    if (cls == null) {
      try {
        // Try resolving path as a class
        cls = MacroTools.pathToClass(p);
        fieldName = null;
      } catch (e: Dynamic) {
        // Try resolving path as a class field
        if (p.length == 1) {
          error();
        }
        cls = MacroTools.pathToClass(p.slice(0, -1));
        fieldName = p[p.length - 1];
      }
    }

    if (fieldName == null) {
      var path = cls.pack.concat([cls.name]);
      return {classType: cls, classPath: path, field: null, isStatic: true};
    }

    // Retrieve field from class by name.
    var cls = CLASSES_INFO.compute(cls);
    var field: Null<ClassField> = cls.statics.get(fieldName);
    var isStatic: Bool = true;

    if (field == null) {
      field = cls.fields.get(fieldName);
      if (field == null) {
        error();
      }
      isStatic = false;
    }

    return {classType: cls.cls, classPath: cls.path, field: field, isStatic: isStatic};
  }

  private static function checkType(e: Expr, type: ComplexType): Expr {
    return {
      expr: ExprDef.ECheckType(e,type),
      pos: e.pos,
    };
  }

  private static function getFieldsFrom(target: ResolvedPath): Array<String> {
    var fields: Array<String> = target.classPath.slice(1); // Skip 'hf'
    if (target.field != null) {
      fields.push(target.field.name);
    }
    return fields;
  }

  private static function functionTypeToComplexType(type: Type, path: Array<String>, pos: Position): ComplexFunctionType {
    return switch (type) {
      case Type.TFun(args, ret):
        var argTypes: Array<ComplexType> = args.map(function(arg) {
          var argType: Null<ComplexType> = TypeTools.toComplexType(arg.t);
          if (argType == null) {
            Context.error("Failed to resolve type for argument `" + arg.name + "` of " + path.join("."), pos);
          }

          // Wrap optional arguments in `Null<T>`
          if (arg.opt) {
            argType = macro: Null<$argType>;
          }          
          return argType;
        });

        var retType: Null<ComplexType> = TypeTools.toComplexType(ret);
        if (retType == null) {
          Context.error("Failed to resolve return type of " + path.join("."), pos);
        }

        {
          params: argTypes,
          ret: retType
        };

      case _: Context.error("Method field has non-function type: " + path.join("."), pos);
    };

  }

  // Erase type parameters to their constraints; this isn't exactly correct, but should
  // be good enough for most use cases (proper typings would require rank-2 polymorphism).
  private static function eraseTypeParameters(type: Type, params: Array<TypeParameter>, pos: Position): Type {
    var dynamicTy: Null<Type> = null;

    var erased = [for (param in params) {
      var constraints = switch (param.t) {
        case TInst(_.get() => { kind: KTypeParameter(c) }, []): c;
        default: throw "Couldn't extract type parameter constraints";
      };

      switch (constraints) {
        case []:
          if (dynamicTy == null)
            dynamicTy = Context.getType("Dynamic");
          dynamicTy;
        case [c]: c;
        case _: Context.error("Type parameters with multiple constraints aren't supported", pos);
      }
    }];

    return TypeTools.applyTypeParameters(type, params, erased);
  }
}

private typedef ClassInfo = {
  cls: ClassType,
  path: Array<String>,
  fields: Map<String, ClassField>,
  statics: Map<String, ClassField>,
};

private typedef ResolvedPath = {
  classType: ClassType,
  classPath: Array<String>,
  field: Null<ClassField>,
  isStatic: Bool, // ignored if `field` is null
};

private typedef ComplexFunctionType = {
  params: Array<ComplexType>,
  ret: ComplexType
};
#end
