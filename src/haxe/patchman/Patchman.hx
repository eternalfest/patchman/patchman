package patchman;

import etwin.Error;
import etwin.Obfu;
import patchman.di.CoreValue;
import patchman.di.ModuleLoader;
import patchman.module.Console;
import Reflect;

/**
  Patchman framework to ease the application of modifications to the game engine.
**/
class Patchman {
  /**
    Supported version of the "engine-patcher" protocol.

    @see https://gitlab.com/eternalfest/specs
  **/
  public static inline var ENGINE_PATCHER_VERSION: String = "3.0.0";

  public static function builder(): PatchmanBuilder {
    patchman.flash.Magic.setup(["hf"]);
    return new PatchmanBuilder();
  }

  public static function bootstrap(mainModuleFactory: Class<Dynamic>): Void {
    builder().create(mainModuleFactory).enable();
  }

  /**
    Factory for the main module.

    Provided during the bootstrap.
  **/
  private var mainModuleFactory: Class<Dynamic>;

  /**
    User modules used to initialize the module cache.
  **/
  private var initialModules: Array<CoreValue>;

  /**
    Reference for the main module once it is created.
  **/
  private var mainModule: Dynamic;

  /**
   Create a new patchman instance for the supplied entry point.
  **/
  @:allow(patchman.PatchmanBuilder)
  private function new(mainModuleFactory: Class<Dynamic>, ?initialModules: Array<CoreValue>) {
    this.mainModuleFactory = mainModuleFactory;
    this.initialModules = initialModules != null ? initialModules : [];
  }

  /**
    Enable Patchman.

    This configures this Flash movie for the "engine-patcher" protocol so it can respond to loader
    requests to patch the game engine.
    Once the loader requests the game to be patched, this will resolve the dependencies of the main
    module and then instantiate it.
  **/
  public function enable() {
    var target: Dynamic =
      #if flash8
      flash.Lib.current;
      #else
      Patchman.crash("NonSupportedPlatform");
      #end

    if (Reflect.hasField(target, Obfu.raw("ENGINE_PATCHER_VERSION"))) {
      Patchman.crash("PatchmanAlreadyEnabled");
    }

    Reflect.setField(target, Obfu.raw("ENGINE_PATCHER_VERSION"), Patchman.ENGINE_PATCHER_VERSION);
    Reflect.setField(target, Obfu.raw("main"), this.patch);
  }

  private static function crash(msg: String): Dynamic {
    patchman.DebugConsole.error(msg);
    throw new Error(msg);
    return null;
  }

  /**
   Handler for the loader request to patch the game.
  **/
  public function patch(hml: HostModuleLoader, done: Null<Error> -> Void): Void {
    var console: Console = Console.fromHml(hml);
    var hf: hf.Hf = hml.require(Obfu.raw("game-engine"));

    var coreValues: Array<CoreValue> = this.initialModules.concat([
      CoreValue.fromDotPath(["hf", "Hf"].join("."), hf),
      CoreValue.fromDotPath(["patchman", "HostModuleLoader"].join("."), hml),
    ]);

    DebugConsole.setConsole(console);
    try {
      this.mainModule = ModuleLoader.load(this.mainModuleFactory, coreValues);
      done(null);
    } catch (err: Dynamic) {
      console.error(err);
      if (Reflect.hasField(err, "message")) {
        done(err);
      } else {
        var msg: String = "An unknown error occured during the patching.";
        done(new Error(msg));
      }
    }
  }

  public static function patchAll(patches: Array<IPatch>, hf: hf.Hf): Bool {
    var result: Bool = false;
    for (patch in patches) {
      var patchResult: Bool = patch.patch(hf);
      result = result || patchResult;
    }
    return result;
  }

  public static function deepField(o: Dynamic, fields: Array<String>): Dynamic {
    var cur: Dynamic = o;
    for (field in fields) {
      if (cur == null) {
        break;
      }
      cur = Reflect.field(cur, field);
    }
    return cur;
  }
}

class PatchmanBuilder {

  private var initialModules: Array<CoreValue>;
  
  @:allow(patchman.Patchman)
  private function new() {
    this.initialModules = [];
  }

  /**
    Inject a user module into the module cache.

   This module will be available as a dependency for other modules.
  **/
  public function inject<T>(cls: Class<T>, module: T): PatchmanBuilder {
    var clsName: String = Type.getClassName(cls);
    this.initialModules.push(CoreValue.fromDotPath(clsName, module));
    return this;
  }

  /**
    Define the main module and return a `Patchman` instance.
  **/
  public function create(mainModuleFactory: Class<Dynamic>): Patchman {
    return new Patchman(mainModuleFactory, this.initialModules);
  }
}
