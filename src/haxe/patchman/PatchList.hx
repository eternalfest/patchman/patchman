package patchman;

import patchman.OncePatch;

class PatchList extends OncePatch implements IPatch {
  private var patches(default, null): Array<IPatch>;

  public function new(patches: Array<IPatch>): Void {
    super();
    this.patches = patches;
  }

  override function apply(hf: hf.Hf): Void {
    for (patch in patches) {
      patch.patch(hf);
    }
  }
}
