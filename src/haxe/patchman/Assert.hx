package patchman;

import haxe.macro.Expr;
import haxe.macro.ExprTools;

class Assert {
  /**
    Asserts at runtime that `e` is truthy.

    If the test succeeds, the program continues.
    If the test fails, an error is thrown. Its message starts with
    `AssertionError`, followed by the test expression, optionally followed by
    a custom message. If a custom message is provided, it is only evaluated in
    case of an error. It means that it should be OK to call a complex function
    to generate this message as it should be rare.

    This macro should be used to define debug assertions. They are intended to
    check fatal programmer errors such as contract violations. A correct
    program shouldn't ever fail a debug assertion test. A program should never
    use a try/catch to recover from assertion errors: debug assertions can be
    eliminated when compiling with optimizations.

    Examples:
    ```
    // Check a precondition (index in bounds)
    public static function getMessage(messages: Array<String>, index: Int) {
      Assert.debug(0 <= index && index < messages.length);
      return messages[index];
    }
    ```

    Example:
    ```
    // Check a precondition and print a custom message
    public static function getMessage(messages: Array<String>, index: Int) {
      Assert.debug(
        0 <= index && index < messages.length,
        "IndexOutOfBounds when accessing item " + index + " from array with length " + array.length,
      );
      return messages[index];
    }
   ```
  **/
  // TODO: No-op in release build
  public static macro function debug(e: Expr, args: Array<Expr>): Expr {
    var message: String = "AssertionError: " + ExprTools.toString(e);
    if (args.length == 0) {
      return macro if (!$e) { throw new etwin.Error($v{message}); };
    } else {
      var msg = args[0];
      return macro if (!$e) { throw new etwin.Error($v{message} + ": " + ($msg)); };
    }
  }
}
