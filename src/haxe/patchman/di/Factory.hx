package patchman.di;

enum Factory {
  Constructor(_class: Class<Dynamic>);
  Function(_class: Class<Dynamic>, name: String);
  Value(value: Dynamic);
}
