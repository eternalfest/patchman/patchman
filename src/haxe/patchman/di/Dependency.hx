package patchman.di;

enum Dependency {
  /**
    Dependency on a class instance.

    The dot path must reference a class.

   The dependency injection system will resolve it with an instance of this
   class.
  **/
  Simple(classPath: DotPath);

  /**
    Dependency on an array of interfaces.

    The dot path must reference an interface.

    If a module has an array dependency, the path from this module to the to
    the top module must be unique.

    The dependency injection system will resolve it with based on the visible
    dependencies: simple sibling dependencies, and simple dependencies shared
    by the dependent module.
    From these visible dependencies, the resolved array has the following
    values:
    - resolved visible dependencies if their class implements the interface
    - TODO: variables exported by visible dependencies if their type is the interface or extends it

    The values in the array are unique.
  **/
  Array(ifacePath: DotPath);
}
