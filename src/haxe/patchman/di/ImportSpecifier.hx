package patchman.di;

enum ImportSpecifier {
  /**
    Import the whole module.
  **/
  All(classPath: DotPath);

  /**
   Import a (non-static) simple field from a module.
  **/
  Field(classPath: DotPath, name: String);

  /**
   Import a (non-static) iterable field from a module.
  **/
  IterableField(classPath: DotPath, name: String);
}

