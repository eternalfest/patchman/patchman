package patchman.di;

import etwin.Error;
import etwin.Obfu;
import patchman.di.ImportSpecifier;
import etwin.ds.ReadOnlyArray;
import etwin.ds.ReadOnlySet;
import etwin.ds.Set;

/**
  Internal module loader.
**/
class ModuleLoader {
  public static inline var SYMBOL_DI_RECORD: String = "__diRecord";

  public static function load(
    mainClass: Class<Dynamic>,
    coreValues: Array<CoreValue>
  ): Dynamic {
    var graph: DependencyGraph = DependencyGraph.fromMainClass(mainClass, coreValues);
    var cache: Map<DotPath, Dynamic> = new Map();

    for (mod in graph.getSorted()) {
      var value: Dynamic = null;
      if (cache.exists(mod.path)) {
        value = cache.get(mod.path);
      } else {
        value = loadModuleRecord(mod, graph, cache);
        cache.set(mod.path, value);
      }
      if (mod == graph.getMain()) {
        return value;
      }
    }
    throw new Error(Obfu.raw("AssertUnreachable"));
  }

  private static function loadModuleRecord(record: ModuleRecord, graph: DependencyGraph, cache: Map<DotPath, Dynamic>): Dynamic {
    var dependencies: Array<Dynamic> = [];
    for (dep in record.dependencies) {
      switch (dep) {
        case Dependency.Simple(depPath):
          dependencies.push(getModuleFromCache(depPath, graph, cache));
        case Dependency.Array(depPath):
          var arrayImports: ReadOnlySet<ImportSpecifier> = graph.getArrayImports(record.path, depPath);
          var values: Array<Dynamic> = [];
          for (arrayImport in arrayImports) {
            switch (arrayImport) {
              case ImportSpecifier.All(classPath): {
                values.push(getModuleFromCache(classPath, graph, cache));
              }
              case ImportSpecifier.Field(classPath, name): {
                var classMod = getModuleFromCache(classPath, graph, cache);
                var value: Dynamic = Reflect.field(classMod, name);
                Assert.debug(value != null);
                values.push(value);
              }
              case ImportSpecifier.IterableField(classPath, name): {
                var classMod = getModuleFromCache(classPath, graph, cache);
                var valueIterable: Iterable<Dynamic> = Reflect.getProperty(classMod, name);
                for (value in valueIterable) {
                  Assert.debug(value != null);
                  values.push(value);
                }
              }
            }
          }
          dependencies.push(values);
      }
    }

    return switch(record.factory) {
      case Factory.Constructor(_class):
        Type.createInstance(_class, dependencies);
      case Factory.Function(_class, fName):
        Reflect.callMethod(_class, Reflect.field(_class, fName), dependencies);
      case Factory.Value(value):
        return value;
    }
  }

  private static function getModuleFromCache(path: DotPath, graph: DependencyGraph, cache: Map<DotPath, Dynamic>): Dynamic {
    if (graph.getByPath(path) == null) {
      throw new Error("ModuleLoader: UnknownModule: " + path);
    }
    if (!cache.exists(path)) {
      throw new Error("ModuleLoader: ModuleNotInCache: " + path);
    }
    return cache.get(path);
  }
}
