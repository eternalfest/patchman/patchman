package patchman.di;

import patchman.di.DotPath;

/**
  Represents a core module with its value.
**/
class CoreValue {
  public var module(default, null): DotPath;
  public var value(default, null): Dynamic;

  private function new(module: DotPath, value: Dynamic): Void {
    this.module = module;
    this.value = value;
  }

  public static function fromDotPath(dotPath: DotPath, value: Dynamic): CoreValue {
    return new CoreValue(dotPath, value);
  }

  public function toModuleRecord(): ModuleRecord {
    return {
      path: this.module,
      factory: Factory.Value(this.value),
      dependencies: etwin.ds.FrozenArray.empty(),
      interfaces: etwin.ds.FrozenSet.empty(),
      exports: etwin.ds.FrozenMap.empty(),
      iterableExports: etwin.ds.FrozenMap.empty(),
      shareDependencies: false,
    };
  }
}
