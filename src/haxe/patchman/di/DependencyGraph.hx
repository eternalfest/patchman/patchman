package patchman.di;

import haxe.ds.StringMap;
import etwin.Error;
import etwin.ds.FrozenSet;
import etwin.ds.FrozenArray;
import etwin.ds.FrozenMap;
import etwin.ds.ReadOnlyArray;
import etwin.ds.ReadOnlySet;
import etwin.ds.Set;

using patchman.di.DependencyTools;

class DependencyGraph {
  private var main(default, null): ModuleRecord;
  private var modules(default, null): Map<DotPath, ModuleRecord>;
  private var topologicalSort(default, null): ReadOnlyArray<ModuleRecord>;
  // modName -> ifaceName -> Set<mod>
  private var arrayImports(default, null): Map<DotPath, Map<DotPath, Set<ImportSpecifier>>>;

  private function new(
    main: ModuleRecord,
    modules: StringMap<ModuleRecord>,
    topologicalSort: ReadOnlyArray<ModuleRecord>,
    arrayImports: Map<DotPath, Map<DotPath, Set<ImportSpecifier>>>
  ): Void {
    this.main = main;
    this.modules = modules;
    this.topologicalSort = topologicalSort;
    this.arrayImports = arrayImports;
  }

  public inline function getMain(): ModuleRecord {
    return this.main;
  }

  public inline function getByPath(path: DotPath): Null<ModuleRecord> {
    return this.modules.get(path);
  }

  public inline function getSorted(): ReadOnlyArray<ModuleRecord> {
    return this.topologicalSort;
  }

  public inline function getArrayImports(mod: DotPath, iface: DotPath): ReadOnlySet<ImportSpecifier> {
    var modArrayDeps: Null<Map<DotPath, Set<ImportSpecifier>>> = this.arrayImports.get(mod);
    Assert.debug(modArrayDeps != null);
    var importSpecifiers: Null<Set<ImportSpecifier>> = modArrayDeps.get(iface);
    Assert.debug(importSpecifiers != null);
    return importSpecifiers;
  }

  public static function fromMainPath(mainPath: DotPath, coreValues: Iterable<CoreValue>): DependencyGraph {
    return fromMain(classPathToModule(mainPath), coreValues);
  }

  public static function fromMainClass(mainClass: Class<Dynamic>, coreValues: Iterable<CoreValue>): DependencyGraph {
    return fromMain(classToModule(mainClass), coreValues);
  }

  public static function fromMain(main: ModuleRecord, coreValues: Iterable<CoreValue>): DependencyGraph {
    var modules: Map<DotPath, ModuleRecord> = new Map();
    for (cv in coreValues) {
      if (modules.exists(cv.module)) {
        throw new Error("ModuleLoader: DuplicateCoreValue: " + cv.module);
      }
      modules.set(cv.module, cv.toModuleRecord());
    }

    // Iterative post-order DFS:
    // - explore dependencies
    // - build topological sorting
    // - build tree from main to first import

    // From module to first dependent found (null for main and core values)
    var parents: Map<DotPath, Null<DotPath>> = new Map();
    // Modules sorted topologicaly
    var topoSort: Array<ModuleRecord> = [];
    // Stack of actively explored modules
    var stack: Array<ModuleRecord> = [];
    // From active (in `stack`) or fully-visited module to current number of explored dependencies
    var openSet: Map<DotPath, Int> = new Map();
    // From module to interfacePath
    var arrayImportsToResolve: Map<DotPath, Set<DotPath>> = new Map();

    // Initialize DFS
    modules.set(main.path, main);
    stack.push(main);
    openSet.set(main.path, 0);

    while (stack.length > 0) {
      var mod: ModuleRecord = stack[stack.length - 1];
      var visitedChildren: Null<Int> = openSet.get(mod.path);
      Assert.debug(visitedChildren != null); // Non-null because `mod` is on the stack
      if (visitedChildren >= mod.dependencies.length) {
        stack.pop();
        topoSort.push(mod);
        continue;
      }
      // There are unvisited child dependencies
      var childDep: Dependency = mod.dependencies[visitedChildren];
      openSet.set(mod.path, visitedChildren + 1);

      // Visit child dependency
      switch (childDep) {
        case Dependency.Simple(depPath): {
          // Simple class dependency

          // TODO: Disable check in releas mode
          // Check for cycle
          if (isPathInStack(stack, depPath)) {
            throw makeCycleError(stack, depPath);
          }

          if (!openSet.exists(depPath)) {
            // Discovered a new module
            var dep: Null<ModuleRecord> = modules.get(depPath);
            if (dep == null) {
              // This is not a core module, get its record from the class at the given path.
              dep = classPathToModule(depPath);
              modules.set(depPath, dep);
            }
            stack.push(dep);
            openSet.set(depPath, 0);
            parents.set(depPath, mod.path);
          } else {
            // TODO: Rework context-dependent module checks to handle the whole chain
            // TODO: Disable check in release mode
            if (isUniqueModule(modules.get(depPath))) {
              // Unique module imported multiple times
              throw makeDuplicateUniqueModuleError(stack, parents, depPath);
            }
          }
        }
        case Dependency.Array(depPath): {
          // Array (of interfaces) dependency: no extra dependencies to discover
          var modArrayImports: Null<Set<DotPath>> = arrayImportsToResolve.get(mod.path);
          if (modArrayImports == null) {
            modArrayImports = new Set<DotPath>();
            arrayImportsToResolve.set(mod.path, modArrayImports);
          }
          modArrayImports.add(depPath);
        }
      }
    }

    // Resolve array imports
    var arrayImports: Map<DotPath, Map<DotPath, Set<ImportSpecifier>>> = new Map();
    for (modPath in arrayImportsToResolve.keys()) {
      var ifacePaths: Null<Set<DotPath>> = arrayImportsToResolve.get(modPath);
      Assert.debug(ifacePaths != null);
      var modArrayImports: Map<DotPath, Set<ImportSpecifier>> = new Map();
      for (ifacePath in ifacePaths) {
        modArrayImports.set(ifacePath, resolveArrayDependency(ifacePath, modPath, parents, modules));
      }
      arrayImports.set(modPath, modArrayImports);
    }

    return new DependencyGraph(main, modules, topoSort, arrayImports);
  }

  private static function classPathToModule(classPath: DotPath): ModuleRecord {
    return classToModule(Type.resolveClass(classPath));
  }

  private static function classToModule(_class: Class<Dynamic>): ModuleRecord {
    var record: Null<ModuleRecord> = Reflect.field(_class, ModuleLoader.SYMBOL_DI_RECORD);
    Assert.debug(record != null, "Missing DI record on class " + Type.getClassName(_class));
    return record;
  }

  /**
    Resolves which modules should populate an array dependency
  **/
  private static function resolveArrayDependency(ifacePath: DotPath, modPath: DotPath, parents: Map<DotPath, Null<DotPath>>, modules: Map<DotPath, ModuleRecord>): Set<ImportSpecifier> {
    // Chain of imports from the highest ancestor sharing its dependencies to the module with the array dependency
    var importChain: Array<DotPath> = [];
    var curPath: Null<DotPath> = modPath;
    while (curPath != null) {
      var dependent: Null<ModuleRecord> = modules.get(curPath);
      Assert.debug(dependent != null);
      if (importChain.length > 0 && !dependent.shareDependencies) {
        // Ancestor module that does not share its dependencies
        break;
      }
      importChain.unshift(curPath);
      curPath = parents.get(curPath);
    }

    var result: Set<ImportSpecifier> = new Set<ImportSpecifier>();

    for (i in 0...(importChain.length)) {
      var dependentPath = importChain[i];
      var dependent: Null<ModuleRecord> = modules.get(dependentPath);
      Assert.debug(dependent != null);
      var dependency: Dependency = (i + 1) < importChain.length
      ? Dependency.Simple(importChain[i + 1])
      : Dependency.Array(ifacePath);

      var siblings: Null<{left: Array<Dependency>, right: Array<Dependency>}> = splitDependencies(dependent.dependencies, dependency);
      Assert.debug(siblings != null);

      // `siblings.left` corresponds visible modules shared by `dependent`
      // `siblings.right` contains sibling dependencies imported after the array dependency, they are not allowed to implement the interface

      // TODO: Check that no right sibling implements `iface`

      // Get import specifiers from visible modules
      for (siblingDep in siblings.left) {
        switch (siblingDep) {
          case Dependency.Simple(siblingPath): {
            var siblingMod: Null<ModuleRecord> = modules.get(siblingPath);
            Assert.debug(siblingMod != null);
            result.addAll(findInterfaceImplementations(siblingMod, ifacePath));
          }
          case Dependency.Array(siblingPath): {
            // Don't look inside sibling array dependencies; no implementation
            // will be found here.
          }
        }
      }
    }

    return result;
  }

  private static function findInterfaceImplementations(mr: ModuleRecord, ifacePath: DotPath): Set<ImportSpecifier> {
    var result: Set<ImportSpecifier> = new Set<ImportSpecifier>();
    if (mr.interfaces.exists(ifacePath)) {
      result.add(ImportSpecifier.All(mr.path));
    }
    var validExports: Null<FrozenSet<String>> = mr.exports.get(ifacePath);
    if (validExports != null) {
      for (validExport in validExports) {
        result.add(ImportSpecifier.Field(mr.path, validExport));
      }
    }
    var validIterableExports: Null<FrozenSet<String>> = mr.iterableExports.get(ifacePath);
    if (validIterableExports != null) {
      for (validIterableExport in validIterableExports) {
        result.add(ImportSpecifier.IterableField(mr.path, validIterableExport));
      }
    }
    return result;
  }

  public static function isPathInStack(stack: ReadOnlyArray<ModuleRecord>, path: DotPath): Bool {
    for (mr in stack) {
      if (mr.path == path) {
        return true;
      }
    }
    return false;
  }

  private static function makeCycleError(stack: ReadOnlyArray<ModuleRecord>, depPath: String): Error {
    var chain: ReadOnlyArray<DotPath> = stack
    .map(function(mod: ModuleRecord): DotPath { return mod.path; })
    .concat([depPath]);
    return new Error("ResolutionError: Cycle: " + formatImportChain(chain));
  }

  private static function makeDuplicateUniqueModuleError(stack: ReadOnlyArray<ModuleRecord>, parents: Map<DotPath, Null<DotPath>>, depPath: String): Error {
    var firstChain: Array<DotPath> = [];
    var cur: Null<DotPath> = depPath;
    while (cur != null) {
      firstChain.unshift(cur);
      cur = parents.get(cur);
    }
    var secondChain: ReadOnlyArray<DotPath> = stack
    .map(function(mod: ModuleRecord): String { return mod.path; })
    .concat([depPath]);
    return new Error("ResolutionError: DuplicateUniqueModule: first: " + formatImportChain(firstChain) + ", second: " + formatImportChain(secondChain));
  }

  private static function formatImportChain(chain: ReadOnlyArray<DotPath>): String {
    return chain.join(" | ");
  }

  /**
    Checks if the provided module record is unique.

    A unique module can only be imported once in the whole tree.
  **/
  // TODO rename to "context-dependent' or something like that
  private static function isUniqueModule(mod: ModuleRecord): Bool {
    for (dep in mod.dependencies) {
      switch (dep) {
        case Dependency.Array: return true;
        default: {}
      }
    }
    return false;
  }

  /**
    Searches for `needle` inside `deps`.

    If `needle` is not found, returns `null`.
    If `needle` is found at index `i`, returns `{left: deps.slice(0, i), right: deps.slice(i)}`
  **/
  private static function splitDependencies(deps: ReadOnlyArray<Dependency>, needle: Dependency): Null<{left: Array<Dependency>, right: Array<Dependency>}> {
    for (i in 0...(deps.length)) {
      if (deps[i].equals(needle)) {
        return {left: deps.slice(0, i), right: deps.slice(i)};
      }
    }
    return null;
  }
}
