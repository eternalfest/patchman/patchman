package patchman.di;

import patchman.di.Dependency;

class DependencyTools {
  public static inline function equals(left: Dependency, right: Dependency): Bool {
    return switch(left) {
      case Dependency.Simple(leftPath): {
        switch (right) {
          case Dependency.Simple(rightPath): leftPath == rightPath;
          default: false;
        }
      }
      case Dependency.Array(leftPath): {
        switch (right) {
          case Dependency.Array(rightPath): leftPath == rightPath;
          default: false;
        }
      }
    }
  }
}
