package patchman.di;

import etwin.ds.FrozenArray;
import etwin.ds.FrozenMap;
import etwin.ds.FrozenSet;

import patchman.di.DotPath;

typedef ModuleRecord = {
  var path(default, null): DotPath;
  var factory(default, null): Factory;
  var dependencies(default, null): FrozenArray<Dependency>;
  var interfaces(default, null): FrozenSet<DotPath>;
  var exports(default, null): FrozenMap<DotPath, FrozenSet<String>>;
  var iterableExports(default, null): FrozenMap<DotPath, FrozenSet<String>>;
  var shareDependencies(default, null): Bool;
};
