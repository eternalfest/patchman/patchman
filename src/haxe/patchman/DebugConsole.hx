package patchman;

import patchman.module.Console;

/**
  Static console class available during debug builds.
**/
class DebugConsole {
  private static var history: Array<Console -> Void> = [];
  private static var console: Null<Console>;

  public static function setConsole(console: Null<Console>): Void {
    DebugConsole.console = console;
    if (DebugConsole.console != null && DebugConsole.history.length > 0) {
      for (msg in DebugConsole.history) {
        msg(DebugConsole.console);
      }
      // Empty history
      DebugConsole.history.splice(0, DebugConsole.history.length);
    }
  }

  public static function log(x: Dynamic): Void {
    if (DebugConsole.console != null) {
      DebugConsole.console.log(x);
    } else {
      DebugConsole.history.push(function(c: Console) { c.log(x); });
    }
  }

  public static function warn(x: Dynamic): Void {
    if (DebugConsole.console != null) {
      DebugConsole.console.warn(x);
    } else {
      DebugConsole.history.push(function(c: Console) { c.warn(x); });
    }
  }

  public static function error(x: Dynamic): Void {
    if (DebugConsole.console != null) {
      DebugConsole.console.error(x);
    } else {
      DebugConsole.history.push(function(c: Console) { c.error(x); });
    }
  }

  public static function print(x: Dynamic): Void {
    if (DebugConsole.console != null) {
      DebugConsole.console.print(x);
    } else {
      DebugConsole.history.push(function(c: Console) { c.print(x); });
    }
  }
}
