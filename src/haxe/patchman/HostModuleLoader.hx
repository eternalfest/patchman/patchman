package patchman;

/**
  Represents the module loader provided by the Host.

  During normal runtime it is passed by the `loader` to expose raw modules
  provided by the loader (`run`, `run-start`, `console`, `json`, etc.).
  See `engine-patcher` spec for more information.
**/
extern class HostModuleLoader {
  public function require(id: String): Dynamic;
}
