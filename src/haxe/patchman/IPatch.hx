package patchman;


/**
  Interface representing a modification that can be applied to the game.
**/
interface IPatch {
  /**
    Ensures the patch is applied to the Hammerfest main MovieClip.

    If you call it multiple times on the same MovieClip, only the first call
    can modify the game. This property is called `idempotency`.
    This property helps to ensure ordering of the modification.

    If you write your own classes implementing the `IPatch` interface, you
    *MUST* make sure that you maintain this property.
    It is recommended to avoid implementing this interface unless
    there are no other solutions.

    @param hf Main Hammerfest MovieClip to patch.
    @return Boolean indicating if the MovieClip was modified. The first
    call may return `true` or `false`, subsequent calls always return `false`.
  **/
  public function patch(hf: hf.Hf): Bool;
}
