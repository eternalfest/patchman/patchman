package patchman;

#if macro
import haxe.macro.Expr;
import patchman.macro.BuildImpl;
import patchman.macro.HfClassImpl;

/**
  Build-time macros.
**/
class Build {
  /**
    Generate _Dependency Injection_ metadata at build time.
    This function adds the `__diRecord` static field (typed as
    `patchman.di.PartialModuleRecord`). The value is computed by reading
    the types and metadata of the class.
   
    You can use the following compile time metadata:
    - `@:diFactory`: marks the static function as the factory: it will be
       called by the module loader to create an instance of this module.
       If no function is marked with this metadata, the constructor (`new`)
       is used.
    - `@:diExport`: marks the field as a provider: other modules will be able
       to access its contents by declaring an `Array<T>` dependency.
       The exported type(s) can be modified by specifying them in the metadata
       as arguments: `@:diExport(Type1, ...)`.
  **/
  public static function di(): Array<Field> {
    return BuildImpl.di();
  }


  /**
    This macro is called for all custom classes extending hf.* classes;
    it rewrites the 'super' constructor call to properly instanciate
    the object.
   
    It will report an error if the class isn't marked with the `:hfTemplate` metadata.
  **/
  public static function hfClass(): Null<Array<Field>> {
    return HfClassImpl.hfClass();
  }
}
#end
