package patchman.fpatch;

import patchman.IPatch;
import patchman.OncePatch;

class ReplacePatch extends OncePatch implements IPatch {
  private var parentPath(default, null): Array<String>;
  private var name(default, null): String;
  private var ctxArg(default, null): Bool;
  private var argCount(default, null): Int;
  private var fn(default, null): Dynamic;

  /**
    Creates a patch to replace `path` with the `fn` function.
    Only the original function will be replaced; any other patch will stay active.
    Trying to apply several `ReplacePatch`s on the same path will throw an error.

    The `fn` argument must have one of the following signatures:
    ```
    hf.Hf -> Ctx −> R;
    hf.Hf -> Ctx −> A0 -> R;
    hf.Hf -> Ctx −> A0 -> A1 -> R;
    ...
    ```

    If `ctxArg` is `false`, the `ctx` argument must be omitted.
  **/
  public function new(parentPath: Array<String>, name: String, ctxArg: Bool, argCount: Int, fn: Dynamic) {
    super();
    this.parentPath = parentPath;
    this.name = name;
    this.ctxArg = ctxArg;
    this.argCount = argCount;
    this.fn = fn;
  }

  override function apply(hf: hf.Hf): Void {
    HandlerStore.getOrCreate(hf, this.parentPath, this.name, this.ctxArg, this.argCount).addReplace(this.fn);
  }
}
