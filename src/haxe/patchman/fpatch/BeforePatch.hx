package patchman.fpatch;

import patchman.IPatch;
import patchman.OncePatch;

class BeforePatch extends OncePatch implements IPatch {
  private var parentPath(default, null): Array<String>;
  private var name(default, null): String;
  private var ctxArg(default, null): Bool;
  private var argCount(default, null): Int;
  private var fn(default, null): Dynamic;

  /**
    Creates a patch to update `path` to run `fn` at the start.

    The `fn` argument must have one of the following signatures:
    ```
    hf.Hf -> Ctx -> Void;
    hf.Hf -> Ctx −> A0 --> Void;
    hf.Hf -> Ctx −> A0 -> A1 -> Void;
    ...
    ```

    If `ctxArg` is `false`, the `Ctx` argument must be omitted.
  **/
  public function new(parentPath: Array<String>, name: String, ctxArg: Bool, argCount: Int, fn: Dynamic) {
    super();
    this.parentPath = parentPath;
    this.name = name;
    this.ctxArg = ctxArg;
    this.argCount = argCount;
    this.fn = fn;
  }

  override function apply(hf: hf.Hf): Void {
    return HandlerStore.getOrCreate(hf, this.parentPath, this.name, this.ctxArg, this.argCount).addBefore(this.fn);
  }
}
