package patchman.fpatch;

import etwin.Error;
import etwin.ds.Nil;

class HandlerStore {
  public static inline var STORE_SYMBOL: String = "__pmStore";

  /**
    Reference to the main Hammerfest MovieClip owning the function.
  **/
  private var hf(default, null): hf.Hf;

  /**
    Pass the context as an explicit argument.
  **/
  private var contextArg(default, null): Bool;

  /**
    Expected number of arguments.

    Used to ensure that the result is at the right position for the `after`
    and `postfix` handlers.
  **/
  private var argCount(default, null): Int;

  /**
    Current base function.

    It represents the last `replace` applied, or the original function.
  **/
  private var base(default, null): Dynamic;

  /**
    Previous base functions.

    When `base` changes following a `replace`, the previous value is pushed
    at the end of this array.
    These functions are never called. This field may some day serve to revert
    changes or provide better error messages.
  **/
  private var oldBases(default, null): Array<Dynamic>;

  /**
    List of handlers running before `base`.

    Stored in reverse order; the first function to run is at the end of the array.
  **/
  private var before: Array<Dynamic>;

  /**
    Controls the short-circuiting behavior of the `before` handlers.

    For a given handler:
      - if `beforeSkip` is negative, it never short-circuits, and its
        return value is ignored;
      - else, it short-circuits when its return vale is not `Nil.none()`,
        and jump to the `after` handler with the given index.
  **/
  private var beforeSkip: Array<Int>;

  /**
    Indicate the type of each `before` handlers.

    - Handlers added with `addPrefix` go before the boundary;
    - Handlers added with `addBefore` go after the boundary.

    Since `before` handlers are stored in reverse order, `before` handlers
    will execute before `prefix` handlers.
  **/
  private var beforeBoundary: Int = 0;

  /**
    List of handlers running after `base`.

    Stored in forward order; the first handler to run is at the start of the array.
  **/
  private var after: Array<Dynamic>;

  /**
    Indicate the type of each `after` handlers.

    - Handlers added with `addPostfix` go before the boundary;
    - Handlers added with `addAfter` go after the boundary.

    Handlers before the boundary can override the return value of
    the whole function.
  **/
  private var afterBoundary: Int = 0;

  public function new(hf: hf.Hf, contextArg: Bool, argCount: Int, base: Dynamic) {
    this.hf = hf;
    this.contextArg = contextArg;
    this.argCount = argCount;

    this.base = base;
    this.oldBases = [];
    this.before = [];
    this.beforeSkip = [];
    this.after = [];
  }

  public function addReplace(fn: Dynamic): Void {
    if (this.oldBases.length == 0) {
      // First replace
      this.oldBases.push(this.base);
      this.base = fn;
    } else {
      throw new Error("!ReplaceConflict");
    }
  }

  public function addWrap(fn: Dynamic): Void {
    // We implement the wrapper as an unconditionally short-circuiting prefix handler
    var wrapped: Dynamic = this.createPartialRunner(this.contextArg, this.beforeBoundary, this.afterBoundary);
    var argsLen = this.argCount + (this.contextArg ? 2 : 1);
    this.addPrefix(function() {
      var args: Array<Dynamic> = untyped __arguments__;
      untyped args.length = argsLen; // force args length to expected size
      args.push(wrapped);
      // TODO: is this correct for static methods relying on `this`? (e.g. `Std.attachMC`)
      return Reflect.callMethod(untyped __this__, fn, args);
    });
  }

  public function addBefore(fn: Dynamic): Void {
    this.before.push(fn);
    // Never short-circuits
    this.beforeSkip.push(-1);
  }

  public function addAfter(fn: Dynamic): Void {
    this.after.push(fn);
  }

  public function addPrefix(fn: Dynamic): Void {
    this.before.insert(this.beforeBoundary, fn);
    // Jump to the first `after` function if this short-circuits
    this.beforeSkip.insert(this.beforeBoundary++, this.afterBoundary);
  }

  public function addPostfix(fn: Dynamic): Void {
    this.after.insert(this.afterBoundary++, fn);
  }

  public function createRunner(): Dynamic {
    return function(): Dynamic {
      return HandlerStore.run(this, this.before.length, this.after.length, untyped __this__, untyped __arguments__);
    };
  }

  private function createPartialRunner(ctxArg: Bool, beforeFrom: Int, afterUntil: Int): Dynamic {
    if (ctxArg) {
      return function(): Dynamic {
        var args: Array<Dynamic> = untyped __arguments__;
        var ctx: Dynamic = args.shift();
        return HandlerStore.run(this, beforeFrom, afterUntil, ctx, args);
      };
    } else {
      return function(): Dynamic {
        return HandlerStore.run(this, beforeFrom, afterUntil, untyped __this__, untyped __arguments__);
      };
    }
  }

  public static function getOrCreate(
    hf: hf.Hf,
    parentPath: Array<String>,
    name: String,
    ctxArg: Bool,
    argCount: Int
  ): HandlerStore {
    var fn: Dynamic = SuperBinding.getFn(hf, parentPath, name);
    var store: Null<HandlerStore> = Reflect.field(fn, HandlerStore.STORE_SYMBOL);
    if (store == null) {
      store = new HandlerStore(hf, ctxArg, argCount, fn);
      fn = store.createRunner();
      Reflect.setField(fn, HandlerStore.STORE_SYMBOL, store);
      // Make the property non-enumerable
      untyped ASSetPropFlags(fn, HandlerStore.STORE_SYMBOL, 1, 0);
      SuperBinding.setFn(hf, parentPath, name, fn);
    }
    return store;
  }

  private static inline function run(store: HandlerStore, beforeFrom: Int, afterUntil: Int, ctx: Dynamic, args: Array<Dynamic>): Dynamic {
    var result: Dynamic = null;
    var afterFrom: Int = -1;

    // Prepare argument list
    var newArgs = args.copy();
    var extraArgs = 1;
    if (store.contextArg) {
      newArgs.unshift(ctx);
      extraArgs++;
    }
    newArgs.unshift(store.hf);

    // Execute the `before` handlers. If a handler short-circuits, we save the
    // returned result and we skip execution to the corresponding `after` handler.
    var beforeHandlers = store.before;
    var beforeSkips = store.beforeSkip;
    while (beforeFrom-- > 0) {
      var shortCircuitResult: Nil<Dynamic> = Reflect.callMethod(ctx, beforeHandlers[beforeFrom], newArgs);
      afterFrom = beforeSkips[beforeFrom];
      if (afterFrom < 0 || shortCircuitResult.isNone()) {
        afterFrom = -1;
      } else {
        result = shortCircuitResult;
        break;
      }
    }

    // Execute the base function, if it wasn't short-circuited
    if (afterFrom < 0) {
      afterFrom = 0;
      // Use the original args if we are calling the original function
      result = Reflect.callMethod(ctx, store.base, store.oldBases.length == 0 ? args : newArgs);
    }

    // Execute the `after` handlers
    if(afterFrom < afterUntil) {
      var resultIdx = store.argCount + extraArgs;
      untyped newArgs.length = resultIdx; // force args len to expected size
      newArgs.push(result);

      var afterHandlers = store.after;
      var boundary = store.afterBoundary;
      do {
        var afterResult: Dynamic = Reflect.callMethod(ctx, afterHandlers[afterFrom], newArgs);
        // Only thread results for `postfix` handlers
        if (afterFrom++ < boundary) {
          newArgs[resultIdx] = result = afterResult;
        }
      } while(afterFrom < afterUntil);
    }

    return result;
  }
}
