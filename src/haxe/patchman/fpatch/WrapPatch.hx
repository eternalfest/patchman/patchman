package patchman.fpatch;

import patchman.IPatch;
import patchman.OncePatch;

class WrapPatch extends OncePatch implements IPatch {
  private var parentPath(default, null): Array<String>;
  private var name(default, null): String;
  private var ctxArg(default, null): Bool;
  private var argCount(default, null): Int;
  private var fn(default, null): Dynamic;

  /**
    Creates a patch to wrap `path` with the `fn` function.
    The last argument passed to `fn` can be used to call the original
    function with any desired arguments, and to capture its return value.
    Only the base function (eventually already wrapped with another
    `WrapPatch`, `PrefixPatch` or `PostfixPatch`) will be replaced; any
    `BeforePatch` or `AfterPatch` will execute as before.

    The `fn` argument must have one of the following signatures:
    ```
    hf.Hf -> Ctx −> (Ctx -> R) -> Void;
    hf.Hf -> Ctx −> A0 -> (Ctx -> A0 -> R) -> Void;
    hf.Hf -> Ctx −> A0 -> (Ctx -> A0 -> A1 -> R) -> R -> Void;
    ...
    ```

    If `ctxArg` is `false`, the `ctx` argument must be omitted.
  **/
  public function new(parentPath: Array<String>, name: String, ctxArg: Bool, argCount: Int, fn: Dynamic) {
    super();
    this.parentPath = parentPath;
    this.name = name;
    this.ctxArg = ctxArg;
    this.argCount = argCount;
    this.fn = fn;
  }

  override function apply(hf: hf.Hf): Void {
    HandlerStore.getOrCreate(hf, this.parentPath, this.name, this.ctxArg, this.argCount).addWrap(this.fn);
  }
}
