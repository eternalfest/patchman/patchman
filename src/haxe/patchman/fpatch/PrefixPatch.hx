package patchman.fpatch;

import patchman.IPatch;
import patchman.OncePatch;

class PrefixPatch extends OncePatch implements IPatch {
  private var parentPath(default, null): Array<String>;
  private var name(default, null): String;
  private var ctxArg(default, null): Bool;
  private var argCount(default, null): Int;
  private var fn(default, null): Dynamic;

  /**
    Creates a patch to prefix `path` with the `fn` function.
    This is exactly equivalent to the following (but with better performance):
    ```
    .wrap(function(hf, args..., old) {
      return fn(hf, args...).isNone() ? old(args...) : result.unwrap();
    })
    ```

    The `fn` argument must have one of the following signatures:
    ```
    hf.Hf -> Ctx −> Nil<R>;
    hf.Hf -> Ctx −> A0 -> Nil<R>;
    hf.Hf -> Ctx −> A0 -> A1 -> Nil<R>;
    ...
    ```

    If `ctxArg` is `false`, the `ctx` argument must be omitted.
  **/
  public function new(parentPath: Array<String>, name: String, ctxArg: Bool, argCount: Int, fn: Dynamic) {
    super();
    this.parentPath = parentPath;
    this.name = name;
    this.ctxArg = ctxArg;
    this.argCount = argCount;
    this.fn = fn;
  }

  override function apply(hf: hf.Hf): Void {
    return HandlerStore.getOrCreate(hf, this.parentPath, this.name, this.ctxArg, this.argCount).addPrefix(this.fn);
  }
}
