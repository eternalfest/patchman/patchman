package patchman.fpatch;

import etwin.ds.WeakMap;
import patchman.fpatch.SuperBindingTable;

class SuperBinding {
  private static var BINDING_STATES: WeakMap<hf.Hf, Map<String, Float>> = new WeakMap();

  // Returns the requested the function, ensuring it is safe with regard to super bindings
  public static function getFn(hf: hf.Hf, parentPath: Array<String>, name: String): Dynamic {
    var parent: Null<Dynamic> = Patchman.deepField(hf, parentPath);
    var fn: Dynamic = Reflect.field(parent, name);
    Assert.debug(fn != null);
    var isMethod: Bool = parentPath[parentPath.length - 1] == "prototype";
    if (!isMethod) {
      // Nothing to do for static functions
      return fn;
    }
    if (untyped !parent.hasOwnProperty(name)) {
      // Inherited method: set it explicitly
      fn = function() {
        return untyped parent.__proto__[name].call(__this__, __arguments__);
      }
      Reflect.setField(parent, name, fn);
    }
    var classPath: Array<String> = parentPath.slice(0, -1); // Drop `prototype`
    var dynBindings: Null<Dynamic> = Patchman.deepField(SuperBindingTable.READ_DYN, classPath);
    if (dynBindings == null) {
      // No dynamic bindings for this class
      return fn;
    }
    var dynBinding: Null<String> = Reflect.field(dynBindings, name);
    if (dynBinding == null) {
      // No dynamic binding triggered by this class/method
      return fn;
    }
    ensureDynamicBinding(hf, name, dynBinding);
    // Reload `fn` after dynamic bindings are applied
    var fn = Reflect.field(parent, name);
    return fn;
  }

  // Updates the requested the function, ensuring it is safe with regard to super bindings
  public static function setFn(hf: hf.Hf, parentPath: Array<String>, name: String, fn: Dynamic): Void {
    var parent: Null<Dynamic> = Patchman.deepField(hf, parentPath);
    Assert.debug(parent != null);
    Reflect.setField(parent, name, fn);
    var isMethod: Bool = parentPath[parentPath.length - 1] == "prototype";
    if (!isMethod) {
      // Nothing to do for static functions
      return;
    }
    var classPath: Array<String> = parentPath.slice(0, -1); // Drop `prototype`
    var classBindings: Null<Dynamic> = Patchman.deepField(SuperBindingTable.WRITE_VAR, classPath);
    if (classBindings == null) {
      // No variable bindings to update for this class
      return;
    }
    var varBinding: Null<Dynamic> = Reflect.field(classBindings, name);
    if (varBinding == null) {
      // No variable binding to update
      return;
    }
    var binding: String = varBinding[0];
    var target: Float = varBinding[1];
    var oldState: Float = getBindingState(hf, binding);
    if (target > oldState) {
      Reflect.setField(hf, binding, fn);
      setBindingState(hf, binding, target);
    }
  }

  // -1: Never patched
  // 0, 1, 2, 3...: Var patch
  // Infinity: Dynamic binding
  private static function getBindingState(hf: hf.Hf, binding: String): Float {
    var states: Null<Map<String, Float>> = BINDING_STATES.get(hf);
    if (states == null) {
      states = new Map<String, Float>();
      BINDING_STATES.set(hf, states);
    }
    var state: Null<Float> = states.get(binding);
    return state != null ? state : -1;
  }

  private static function setBindingState(hf: hf.Hf, binding: String, value: Float): Void {
    var states: Null<Map<String, Float>> = BINDING_STATES.get(hf);
    if (states == null) {
      states = new Map<String, Float>();
      BINDING_STATES.set(hf, states);
    }
    states.set(binding, value);
  }

  // Ensure that dynamic binding is enabled for this super binding
  private static function ensureDynamicBinding(hf: hf.Hf, name: String, binding: String): Void {
    var state: Float = getBindingState(hf, binding);
    if (state == Math.POSITIVE_INFINITY) {
      // Dynamic bindings are already applied
      return;
    }
    var classesToPatch: Array<Array<String>> = SuperBindingTable.WRITE_DYN.get(binding);
    Assert.debug(classesToPatch != null);
    for (classPath in classesToPatch) {
      var classProto: Dynamic = Reflect.field(Patchman.deepField(hf, classPath), "prototype");
      var oldFn: Dynamic = Reflect.field(classProto, name);
      var fn = function() {
        untyped hf[binding] = classProto.__proto__[name];
        return untyped oldFn.apply(__this__, __arguments__);
      }
      Reflect.setField(classProto, name, fn);
    }
    setBindingState(hf, binding, Math.POSITIVE_INFINITY);
  }
}
