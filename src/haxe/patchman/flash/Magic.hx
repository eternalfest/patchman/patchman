package patchman.flash;

import etwin.Error;
import etwin.ds.WeakMap;

class Magic {

  public static inline var SYMBOL_TEMPLATE_CLASS_MAP: String = "__patchmanTemplateClassesMap";
  private static inline var SYMBOL_TEMPLATE_CLASS: String = "__patchmanTemplateClass__";

  public static dynamic function setup(mainPack: Array<String>): Void {
    #if !flash8
    throw new Error("Only the flash8 target is supported");
    #else
    protectPackage(mainPack);
    Reflect.setField(flash.Boot, "__instanceof", customHaxeInstanceOf);
    Magic.setup = function(mainPack) {};
    #end
  }
 
  // Disallow any access to the given package.
  // Trying to access an item inside the package will throw an exception.
  public static function protectPackage(path: Array<String>): Void {
    #if !flash8
    throw new Error("Only the flash8 target is supported");
    #else
    if (path.length == 0)
      throw new Error("Invalid package name");

    var msg = 'The package ${path.join(".")} doesn\'t exist at runtime!';
    var error = function() throw new Error(msg);

    path = path.copy();
    var field = path.pop();
    var parent = patchman.Patchman.deepField(flash.Lib.current, path);

    Reflect.deleteField(parent, field);
    parent.addProperty(field, error, error);
    #end
  }

  public static function getOrCreateClassFromTemplate<S, T: S>(template: Class<T>, superClass: Class<S>): Class<T> {
    #if !flash8
    throw new Error("Only the flash8 target is supported");
    #else
    var cache: Null<WeakMap<Class<S>, Class<T>>> = Reflect.field(template, Magic.SYMBOL_TEMPLATE_CLASS_MAP);
    if (cache == null)
      throw new Error("The given class cannot be used as template");

    if (cache.exists(superClass))
      return cache.get(superClass);

    // Make sure the super class' prototype has a constructor field
    var superProto = Reflect.field(superClass, "prototype");
    Reflect.setField(superProto, "constructor", superClass);
    Reflect.setField(superProto, "__class__", superClass); // haxe field

    var generatedClass: Class<T> = untyped {
      // Create class constructor
      var cls = function() {
        Reflect.callMethod(__this__, template, __arguments__);
      };

      cls.__name__ = template.__name__;             // haxe field
      cls.__interfaces__ = template.__interfaces__; // haxe field
      cls[Magic.SYMBOL_TEMPLATE_CLASS] = template;     // custom field
      cls.prototype = {
        __proto__: superClass.prototype,
        constructor: cls,
        __class__: cls, // haxe field
      };

      // Copy instance fields definitions; we use raw access instead
      // of Type.getInstanceFields, because we don't want to copy
      // inherited fields (already handled by prototype manipulation).
      for (field in Reflect.fields(template.prototype)) {
        cls.prototype[field] = template.prototype[field];
      }

      // TODO: should we copy static fields ??
      return cls;
    };

    cache.set(superClass, generatedClass);
    return generatedClass;
    #end
  }

  #if flash8
  // Modification of Std.is to add support for "templated" classes.
  private static function customHaxeInstanceOf(o: Dynamic, cl: Dynamic): Bool {
    // Copy of original Haxe source; see https://github.com/HaxeFoundation/haxe/blob/3.1_bugfix/std/flash8/Boot.hx#L124
    untyped {
      if( !cl )
        return false;
      if( __instanceof__(o,cl) ) {
        if( cl == Array )
          return ( o[__unprotect__("__enum__")] == null );
        return true;
      }

      switch( cast cl ) {
      case Int:
        return __typeof__(o) == "number" && __physeq__(Math.ceil(o),o%2147483648.0) && !(__physeq__(o,true) || __physeq__(o,false));
      case Float:
        return __typeof__(o) == "number";
      case Bool:
        return __physeq__(o,true) || __physeq__(o,false);
      case String:
        return __typeof__(o) == "string";
      case Dynamic:
        return true;
      default:
        if (o[__unprotect__("__enum__")] == cl ||
          (cl == Class && o[__unprotect__("__name__")] != null) ||
          (cl == Enum && o[__unprotect__("__ename__")] != null))
          return true;

        // ADDED CODE
        if (!cl[Magic.SYMBOL_TEMPLATE_CLASS_MAP])
          return false;

        while (true) {
          o = o.__proto__;
          var template = o.constructor[Magic.SYMBOL_TEMPLATE_CLASS];
          if (!template) return false;
          if (template == cl) return true;
        }
      }
    }
  }
  #end
}
