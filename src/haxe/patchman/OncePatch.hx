package patchman;

import etwin.Error;
import etwin.ds.WeakSet;
import patchman.IPatch;

/**
  Helper class to automatically manage IPatch idempotence.

  Subclasses should override the `apply(hf: hf.Hf): Void` method to patch the game;
  this class will ensure that it is only called once per `hf.Hf` instance.
**/
class OncePatch implements IPatch {
  private var patched(default, null): WeakSet<hf.Hf>;

  private function new(): Void {
    this.patched = new WeakSet();
  }

  @:dox(show)
  private function apply(hf: hf.Hf): Void {
    throw new Error("The 'apply' method should be overriden!");
  }

  public function patch(hf: hf.Hf): Bool {
    if (this.patched.exists(hf)) {
      return false;
    }
    this.apply(hf);
    this.patched.add(hf);
    return true;
  }
}
