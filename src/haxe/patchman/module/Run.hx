package patchman.module;

import ef.api.run.IRun;
import etwin.Obfu;

/**
  Patchman wrapper for the loader's `run` module.
**/
@:build(patchman.Build.di())
class Run {
  private var hostModule: HostModule;

  public function new(hostModule: Dynamic) {
    this.hostModule = hostModule;
  }

  public function getRun(): IRun {
    return this.hostModule.getRun();
  }

  @:diFactory
  public static function fromHml(hml: HostModuleLoader): Run {
    return new Run(hml.require(Obfu.raw("run")));
  }
}

private typedef HostModule = {
  function getRun(): IRun;
};
