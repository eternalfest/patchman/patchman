package patchman.module;

import ef.api.run.ISetRunResultOptions;
import etwin.Obfu;

/**
  Patchman wrapper for the loader's `game-events` module.
**/
@:build(patchman.Build.di())
class GameEvents {
  private var hostModule: HostModule;

  public function new(hostModule: Dynamic) {
    this.hostModule = hostModule;
  }

  public function emitError(error: Dynamic): String {
    return this.hostModule.emitError(error);
  }

  public function endGame(options: ISetRunResultOptions): String {
    return this.hostModule.endGame(options);
  }

  @:diFactory
  public static function fromHml(hml: HostModuleLoader): GameEvents {
    return new GameEvents(hml.require(Obfu.raw("game-events")));
  }
}

private typedef HostModule = {
  function emitError(error: Dynamic): String;
  function endGame(options: ISetRunResultOptions): String;
};
