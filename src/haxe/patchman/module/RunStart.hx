package patchman.module;

import ef.api.run.IRunStart;
import etwin.Obfu;

/**
  Patchman wrapper for the loader's `run-start` module.
**/
@:build(patchman.Build.di())
class RunStart {
  private var hostModule: HostModule;

  public function new(hostModule: Dynamic) {
    this.hostModule = hostModule;
  }

  public function get(): Null<IRunStart> {
    return this.hostModule.get();
  }

  @:diFactory
  public static function fromHml(hml: HostModuleLoader): RunStart {
    return new RunStart(hml.require(Obfu.raw("run-start")));
  }
}

private typedef HostModule = {
  function get(): Null<IRunStart>;
};
