package patchman.module;

import patchman.HostModuleLoader;
import etwin.Obfu;

/**
  Patchman wrapper for the loader's `console` module.
**/
@:build(patchman.Build.di())
class Console {
  private var hostModule: HostModule;

  public function new(hostModule: Dynamic) {
    this.hostModule = hostModule;
  }

  public function log(x: Dynamic): Void {
    this.hostModule.log(x);
  }

  public function warn(x: Dynamic): Void {
    this.hostModule.warn(x);
  }

  public function error(x: Dynamic): Void {
    this.hostModule.error(x);
  }

  public function print(x: Dynamic): Void {
    this.hostModule.print(x);
  }

  public function clear(): Void {
    this.hostModule.clear();
  }

  @:diFactory
  public static function fromHml(hml: HostModuleLoader): Console {
    return new Console(hml.require(Obfu.raw("console")));
  }
}

private typedef HostModule = {
  function log(x: Dynamic): Void;
  function warn(x: Dynamic): Void;
  function error(x: Dynamic): Void;
  function print(x: Dynamic): Void;
  function clear(): Void;
};
