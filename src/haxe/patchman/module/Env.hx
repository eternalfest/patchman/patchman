package patchman.module;

import etwin.Obfu;

/**
 Patchman wrapper for the loader's `env` module.
**/
@:build(patchman.Build.di())
class Env {
  private var hostModule: HostModule;

  public function new(hostModule: Dynamic) {
    this.hostModule = hostModule;
  }

  public function getLoaderVersion(): String {
    return this.hostModule.getLoaderVersion();
  }

  public function isDebug(): Bool {
    return this.hostModule.isDebug();
  }

  @:diFactory
  public static function fromHml(hml: HostModuleLoader): Env {
    return new Env(hml.require(Obfu.raw("env")));
  }
}

private typedef HostModule = {
  function getLoaderVersion(): String;
  function isDebug(): Bool;
};
