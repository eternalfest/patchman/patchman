package patchman.module;

import patchman.HostModuleLoader;
import etwin.Obfu;

/**
  Patchman wrapper for the loader's `data` module.
**/
@:build(patchman.Build.di())
class Data {
  private var hostModule: Dynamic;

  public function new(hostModule: Dynamic) {
    this.hostModule = hostModule;
  }

  public function get(key: String): Dynamic {
    return Reflect.field(this.hostModule, key);
  }

  public function has(key: String): Bool {
    return Reflect.hasField(this.hostModule, key);
  }

  @:diFactory
  public static function fromHml(hml: HostModuleLoader): Data {
    return new Data(hml.require(Obfu.raw("data")));
  }
}
