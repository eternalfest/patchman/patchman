package patchman;

/**
  A patch that doesn't do anything.
**/
class VoidPatch implements IPatch {
  public function new(): Void {}

  public function patch(hf: hf.Hf): Bool {
    return false;
  }
}
