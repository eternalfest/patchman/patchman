package patchman.ref;

import patchman.vpatch.ReplaceVarPatch;
import patchman.vpatch.UpdateVarPatch;

class Var<T> extends Ref {
  public function new(path: Array<String>) {
    super(path, true);
  }

  public function replace(newValue: T): IPatch {
    return new ReplaceVarPatch(this.parentPath, this.name, newValue);
  }

  public function update(updateFn: T -> T): IPatch {
    return new UpdateVarPatch(this.parentPath, this.name, updateFn);
  }
}
