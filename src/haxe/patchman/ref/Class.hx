package patchman.ref;

class Class<T> extends Ref {
  public function new(path: Array<String>) {
    super(path, false);
  }
}
