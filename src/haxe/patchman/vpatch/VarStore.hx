package patchman.vpatch;

import etwin.Error;

class VarStore {
  public static inline var STORE_SYMBOL: String = "__pmVarStores";

  private var parent: Dynamic;
  private var name: String;

  /**
    The original value of the static variable.
  **/
  private var base: Dynamic;

  /**
    Was the variable modified? Used for detecting conflicts.
  **/
  private var isModified: Bool;

  public function new(parent: Dynamic, name: String) {
    this.parent = parent;
    this.name = name;
    this.base = Reflect.field(parent, name);
  }

  public function addReplace(newVal: Dynamic): Void {
    if (this.isModified) {
      // For now `addUpdate()` followed by `addReplace()` is a conflict.
      // To allow this, we would need to store the update functions
      // and re-call them when addReplace is called.
      // TODO: is this a good idea?
      throw new Error("!ReplaceConflict");
    }

    this.isModified = true;
    Reflect.setField(parent, name, newVal);
  }

  public function addUpdate(update: Dynamic -> Dynamic): Void {
    var oldVal = Reflect.field(parent, name);
    var newVal = update(oldVal);
    Reflect.setField(parent, name, newVal);
    this.isModified = true;
  }

  public static function getOrCreate(
    hf: hf.Hf,
    parentPath: Array<String>,
    name: String
  ): VarStore {
    var parent = Patchman.deepField(hf, parentPath);

    var storeMap: Null<Dynamic> = Reflect.field(parent, STORE_SYMBOL);
    if (storeMap == null) {
      storeMap = {};
      Reflect.setField(parent, STORE_SYMBOL, storeMap);
      // Make the property non-enumerable
      untyped ASSetPropFlags(fn, VarStore.STORE_SYMBOL, 1, 0);
    }

    var store: Null<VarStore> = Reflect.field(storeMap, name);
    if (store == null) {
      store = new VarStore(parent, name);
      Reflect.setField(storeMap, name, store);
    }

    return store;
  }
}
