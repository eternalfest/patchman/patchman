package patchman.vpatch;

import patchman.IPatch;
import patchman.OncePatch;

class ReplaceVarPatch extends OncePatch implements IPatch {
  private var parentPath(default, null): Array<String>;
  private var name(default, null): String;
  private var newValue(default, null): Dynamic;

  /**
    Creates a patch to replace the static variable at `path` with a new value.
  **/
  public function new(parentPath: Array<String>, name: String, newValue: Dynamic) {
    super();
    this.parentPath = parentPath;
    this.name = name;
    this.newValue = newValue;
  }

  override function apply(hf: hf.Hf): Void {
    VarStore.getOrCreate(hf, this.parentPath, this.name).addReplace(this.newValue);
  }
}
