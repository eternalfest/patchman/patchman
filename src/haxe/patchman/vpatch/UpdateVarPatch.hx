package patchman.vpatch;

import patchman.IPatch;
import patchman.OncePatch;

class UpdateVarPatch implements IPatch {
  private var parentPath(default, null): Array<String>;
  private var name(default, null): String;
  private var updateFn(default, null): Null<Dynamic -> Dynamic>;

  /**
    Creates a patch to update the value of the static variable at `path` with the given function.
  **/
  public function new(parentPath: Array<String>, name: String, updateFn: Dynamic -> Dynamic) {
    this.parentPath = parentPath;
    this.name = name;
    this.updateFn = updateFn;
  }

  public function patch(hf: hf.Hf): Bool {
    if (this.updateFn == null) {
      return false;
    }

    VarStore.getOrCreate(hf, this.parentPath, this.name).addUpdate(this.updateFn);
    this.updateFn = null;
    return true;
  }
}
