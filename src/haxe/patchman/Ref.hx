package patchman;

import haxe.macro.Expr;
import patchman.macro.RefImpl;

class Ref {
  /**
    Full path

    ```
    // hf.path.to.Class
    ["path", "to", "Class"]
    // hf.path.to.Class.staticField
    ["path", "to", "Class", "staticField"]
    // hf.path.to.Class.method
    ["path", "to", "Class", "prototype", "method"]
    ```
  **/
  public var path(default, null): Array<String>;

  /**
    Path to parent object

    ```
    // hf.path.to.Class
    ["path", "to"]
    // hf.path.to.Class.staticField
    ["path", "to", "Class"]
    // hf.path.to.Class.method
    ["path", "to", "Class", "prototype"]
    ```
  **/
  public var parentPath(default, null): Array<String>;

  /**
    Last part of the path

    ```
    // hf.path.to.Class
    "Class"
    // hf.path.to.Class.staticField
    "staticField"
    // hf.path.to.Class.method
    "method"
    ```
  **/
  public var name(default, null): String;

  /**
    Full path without `prototype`

    ```
    // hf.path.to.Class
    ["path", "to", "Class"]
    // hf.path.to.Class.staticField
    ["path", "to", "Class", "staticField"]
    // hf.path.to.Class.method
    ["path", "to", "Class", "method"]
    ```
  **/
  public var haxePath(default, null): Array<String>;

  public var isStatic(default, null): Bool;

  public function new(haxePath: Array<String>, isStatic: Bool) {
    var name: String = haxePath[haxePath.length - 1];
    var parentPath: Array<String> = haxePath.slice(0, -1).concat(isStatic ? [] : ["prototype"]);
    var path: Array<String> = parentPath.concat([name]);

    this.path = path;
    this.parentPath = parentPath;
    this.name = name;
    this.haxePath = haxePath;
    this.isStatic = isStatic;
  }

  /**
    Call a method from an `hf.*` class using static dispatch.

    Useful for calling `super` methods in a method patch body.

    Usage:
    ```
    import hf.entity.Physics;

    var hf: hf.Hf = ...;
    Ref.call(hf, Physics.update, self); // Explicit class
    Ref.call(hf, super.update, self);   // Use super-class
    Ref.call(hf, this.update, self);    // Use current class
    ```
  **/
  macro public static function call(hf: Expr, ref: Expr, obj: Expr, args: Array<Expr>): Expr {
    return RefImpl.call(hf, ref, obj, args);
  }

  macro public static function auto(e: Expr): Expr {
    return RefImpl.auto(e);
  }
}
