package ef.api.types;

interface IIdRef {
  public var id: String;
}
