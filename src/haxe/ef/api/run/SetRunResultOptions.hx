package ef.api.run;

import ef.api.run.ISetRunResultOptions;

class SetRunResultOptions implements ISetRunResultOptions {
  public var isVictory: Bool;
  public var maxLevel: Int;
  public var scores: Array<Int>;
  public var items: Map<String, Int>;
  public var stats: Dynamic;

  public function new(isVictory: Bool, maxLevel: Int, scores: Array<Int>, items: Map<String, Int>, stats: Dynamic) {
    this.isVictory = isVictory;
    this.maxLevel = maxLevel;
    this.scores = scores;
    this.items = items;
    this.stats = stats;
  }
}
