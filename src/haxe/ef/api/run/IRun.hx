package ef.api.run;

import ef.api.games.IGameRef;
import ef.api.run.IRunSettings;
import ef.api.users.IUserRef;

interface IRun {
  var id: String;
  var createdAt: Date;
  var startedAt: Null<Date>;
  var game: IGameRef;
  var user: IUserRef;
  var gameMode: String;
  var gameOptions: Array<String>;
  var settings: IRunSettings;
}
