import furi from "furi";
import sysPath from "path";
import url from "url";

import meta from "./meta.mjs";

export function getHaxeStdLibUrl(): url.URL {
  return furi.fromSysPath(sysPath.join(meta.dirname, "../haxe"));
}
