import { findUpSync } from "find-up";
import fs from "fs";

import meta from "./meta.mjs";

const packagePath: string | undefined = findUpSync("package.json", {cwd: meta.dirname});

if (packagePath === undefined) {
  throw new Error("Cannot find `package.json`");
}

const pkg: any = JSON.parse(fs.readFileSync(packagePath, {encoding: "utf-8"}));

export const VERSION: string = pkg.version;
