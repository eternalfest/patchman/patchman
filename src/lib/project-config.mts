import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { TsEnumType } from "kryo/ts-enum";
import { Ucs2StringType } from "kryo/ucs2-string";

export enum PackageResolution {
  Node,
}

export const $PackageResolution: TsEnumType<PackageResolution> = new TsEnumType<PackageResolution>({
  enum: PackageResolution,
  changeCase: CaseStyle.KebabCase,
});

export interface ProjectConfig {
  readonly main: string;
  readonly outFile: string;
  readonly outDocDir: string;
  readonly obfuMap: string;
  readonly obfuKey: string;
  readonly packageResolution?: PackageResolution;
}

export const $ProjectConfig: RecordIoType<ProjectConfig> = new RecordType<ProjectConfig>({
  properties: {
    main: {type: new Ucs2StringType({maxLength: Infinity})},
    outFile: {type: new Ucs2StringType({maxLength: Infinity})},
    outDocDir: {type: new Ucs2StringType({maxLength: Infinity})},
    obfuMap: {type: new Ucs2StringType({maxLength: Infinity})},
    obfuKey: {type: new Ucs2StringType({maxLength: Infinity})},
    packageResolution: {type: $PackageResolution, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface PackageJsonConfig {
  haxeMain?: string;
  outFile?: string;
  outDocDir?: string;
  obfuMap?: string;
  packageResolution?: PackageResolution;
}

export const $PackageJsonConfig: RecordIoType<PackageJsonConfig> = new RecordType<PackageJsonConfig>({
  properties: {
    haxeMain: {type: new Ucs2StringType({maxLength: Infinity}), optional: true},
    outFile: {type: new Ucs2StringType({maxLength: Infinity}), optional: true},
    outDocDir: {type: new Ucs2StringType({maxLength: Infinity}), optional: true},
    obfuMap: {type: new Ucs2StringType({maxLength: Infinity}), optional: true},
    packageResolution: {type: $PackageResolution, optional: true},
  },
});
