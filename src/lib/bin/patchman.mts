#!/usr/bin/env node

import { pathToFileURL, URL } from "url";

import { execCli } from "../cli.mjs";

export async function main(): Promise<void | never> {
  const args: string[] = process.argv.slice(2);
  const cwd: URL = pathToFileURL(process.cwd());
  const returnCode: number = await execCli(args, cwd, process);
  if (returnCode !== 0) {
    process.exit(returnCode);
  }
}
