import furi from "furi";
import sysPath from "path";
import url from "url";

import { AnyError, HaxeError, HaxeMessage } from "../diagnostic/errors.mjs";
import { Diagnostic, DiagnosticCategory } from "../diagnostic/report.mjs";
import { SpawnedProcess, SpawnResult } from "../spawned-process.mjs";

export interface HaxeHxmlLine {
  readonly name: string,
  readonly value: string | url.URL | null,
  readonly quoted: boolean,
}

export type HaxeHxml = ReadonlyArray<HaxeHxmlLine>;

export interface HaxeBaseTarget {
  readonly main?: string,
  readonly classPath: ReadonlyArray<url.URL>;
  readonly defines: ReadonlyMap<string, string>;
}

export interface HaxeBuildTarget extends HaxeBaseTarget {
  readonly main: string;
  readonly swfHeader: string;
  readonly outFile: url.URL;
  readonly noOutput: boolean;
}

export interface HaxeXmlTarget extends HaxeBaseTarget {
  readonly packIncludes: ReadonlyArray<string>;
  readonly outFile: url.URL;
}

export function writeHxml(hxml: HaxeHxml, targetDir?: url.URL): string {
  const lines: string[] = [];
  for (const line of hxml) {
    if (line.value === null) {
      lines.push(line.name);
      continue;
    }

    let val: string;
    if (line.value instanceof url.URL) {
      // TODO: Accept a `projectRoot` argument and limit relative paths to those in the root
      if (targetDir === undefined) {
        val = furi.toSysPath(line.value.toString());
      } else {
        val = relativeSysPath(targetDir, line.value);
      }
    } else {
      val = line.value;
    }

    val = line.quoted ? JSON.stringify(val) : val;
    lines.push(line.name + " " + val);
  }

  lines.push("");
  return lines.join("\n");
}

export async function executeHxml(hxml: HaxeHxml): Promise<Diagnostic[]> {
  const args: string[] = [];
  for (const line of hxml) {
    args.push(line.name);
    const value = line.value;
    if (value !== null) {
      if (value instanceof url.URL) {
        args.push(furi.toSysPath(value.toString()));
      } else {
        args.push(value);
      }
    }
  }

  const result: SpawnResult = await new SpawnedProcess("haxe", args, {stdio: "pipe"}).toPromise();
  return diagnoseHaxeResult(result);
}

export function buildTargetToHxml(target: HaxeBuildTarget): HaxeHxml {
  const hxml: HaxeHxmlLine[] = baseTargetToHxml(target);

  hxml.push({ name: "-swf-version", value: "8", quoted: true });
  hxml.push({ name: "-swf-header", value: target.swfHeader, quoted: true });
  hxml.push({ name: "-swf", value: target.outFile, quoted: true });
  hxml.push({ name: "-D", value: "flash_strict", quoted: false });
  hxml.push({ name: "-D", value: "obfu", quoted: false });
  hxml.push({ name: "-dce", value: "full", quoted: false });
  hxml.push({ name: "--no-traces", value: null, quoted: false });
  if (target.noOutput) {
    hxml.push({ name: "--no-output", value: null, quoted: false });
  }

  return hxml;
}

export function xmlTargetToHxml(target: HaxeXmlTarget): HaxeHxml {
  const hxml: HaxeHxmlLine[] = baseTargetToHxml(target);

  for (const include of target.packIncludes) {
    hxml.push({ name: "--macro", value: `include(${JSON.stringify(include)})`, quoted: false });
  }

  hxml.push({ name: "-swf-version", value: "8", quoted: true });
  hxml.push({ name: "-swf", value: "dummy.swf", quoted: true });
  hxml.push({ name: "-D", value: "flash_strict", quoted: false });
  hxml.push({ name: "-D", value: "doc-gen", quoted: false });
  hxml.push({ name: "--no-output", value: null, quoted: false });
  hxml.push({ name: "-xml", value: target.outFile, quoted: true });

  return hxml;
}

function baseTargetToHxml(target: HaxeBaseTarget): HaxeHxmlLine[] {
  const hxml: HaxeHxmlLine[] = [];

  if (target.main !== undefined) {
    hxml.push({ name: "-main", value: target.main, quoted: true });
  }

  for (const cp of target.classPath) {
    hxml.push({ name: "-cp", value: cp, quoted: true });
  }

  for (const [k, v] of target.defines) {
    hxml.push({ name: "-D", value: `${k}=${v}`, quoted: false });
  }

  return hxml;
}

function relativeSysPath(src: url.URL, dest: url.URL): string {
  const relativeSysPath: string = sysPath.relative(furi.toSysPath(src.toString()), furi.toSysPath(dest.toString()));
  return relativeSysPath !== "" ? relativeSysPath : ".";
}

function diagnoseHaxeResult(spawnResult: SpawnResult): Diagnostic[] {
  const hasError: boolean = !(spawnResult.exit.type === "code" && spawnResult.exit.code === 0);
  const diagnostics: Diagnostic[] = [];
  if (spawnResult.stdout.length > 0) {
    diagnostics.push(new HaxeMessage(spawnResult.stdout.toString("utf-8")));
  }
  if (spawnResult.stderr.length > 0) {
    const message: string = spawnResult.stderr.toString("utf-8");
    diagnostics.push(hasError ? new HaxeError(message) : new HaxeMessage(message));
  }
  if (hasError && !diagnostics.some(diag => diag.category === DiagnosticCategory.Error)) {
    diagnostics.push(new AnyError("Haxe compilation failed without message"));
  }
  return diagnostics;
}
