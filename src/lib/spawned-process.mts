import childProcess from "child_process";
import { PassThrough, Transform as TransformStream } from "stream";

export interface SpawnOptions {
  cwd?: string;
  env?: { [key: string]: string };
  stdio?: "inherit" | "pipe";
  /**
   * Run in detached mode. Default: `false`.
   */
  detached?: boolean;
}

export interface SpawnResult {
  stdout: Buffer;
  stderr: Buffer;
  exit: Exit;
}

export type Exit = SignalExit | CodeExit;

export interface CodeExit {
  type: "code";
  code: number;
}

export interface SignalExit {
  type: "signal";
  signal: string;
}

export class SpawnedProcess {
  readonly process: childProcess.ChildProcess;

  private readonly stdoutChunks: Buffer[];
  private readonly stderrChunks: Buffer[];
  private exit?: Exit;

  constructor(file: string, args: string[], options: SpawnOptions) {
    this.stdoutChunks = [];
    this.stderrChunks = [];
    this.exit = undefined;

    const detached: boolean = options.detached !== undefined ? options.detached : false;

    this.process = childProcess.spawn(
      file,
      args,
      {stdio: [process.stdin, "pipe", "pipe"], cwd: options.cwd, env: options.env, detached},
    );

    const stdout: TransformStream = new PassThrough();
    this.process.stdout!.pipe(stdout);
    const stderr: TransformStream = new PassThrough();
    this.process.stderr!.pipe(stderr);
    if (options.stdio === "inherit") {
      stdout.pipe(process.stdout);
      stderr.pipe(process.stderr);
    }

    stdout.on("data", (chunk: Buffer): void => {
      this.stdoutChunks.push(chunk);
    });
    stderr.on("data", (chunk: Buffer): void => {
      this.stderrChunks.push(chunk);
    });

    this.process.once("exit", (code: number | null, signal: string | null): void => {
      if (code !== null) {
        this.exit = {type: "code", code};
      } else {
        this.exit = {type: "signal", signal: signal!};
      }
    });
  }

  async toPromise(): Promise<SpawnResult> {
    return new Promise<SpawnResult>((resolve: (res: SpawnResult) => void, reject) => {
      if (this.exit !== undefined) {
        const [stdout, stderr]: [Buffer, Buffer] = this.getBuffers();
        resolve({stdout, stderr, exit: this.exit});
      } else {
        this.process.once("exit", (code: number | null, signal: string | null): void => {
          let exit: Exit;
          if (code !== null) {
            exit = {type: "code", code};
          } else {
            exit = {type: "signal", signal: signal!};
          }
          const [stdout, stderr]: [Buffer, Buffer] = this.getBuffers();
          resolve({stdout, stderr, exit});
        });

        this.process.once("error", (err: Error): void => {
          reject(err);
        });
      }
    });
  }

  private getBuffers(): [Buffer, Buffer] {
    const stdout: Buffer = Buffer.concat(this.stdoutChunks);
    const stderr: Buffer = Buffer.concat(this.stderrChunks);
    this.stdoutChunks.length = 0;
    this.stderrChunks.length = 0;
    this.stdoutChunks.push(stdout);
    this.stderrChunks.push(stderr);
    return [stdout, stderr];
  }
}
