import stream from "stream";

export type DiagnosticReporter = (diagnostics: ReadonlyArray<Diagnostic>) => void;

export interface Diagnostic {
  readonly code: number;
  readonly message: string;
  readonly category: DiagnosticCategory;
  readonly fileUri?: string;
  readonly sourceText?: string;
  readonly startOffset?: number;
  readonly length?: number;
}

export enum DiagnosticCategory {
  /**
   * Non-fatal issue.
   */
  Warning = "warning",

  /**
   * A fatal error happened: no (or invalid) output.
   */
  Error = "error",

  /**
   * Simple info message.
   */
  Message = "message",
}

export function createTtyReporter(ttyStream: stream.Writable): DiagnosticReporter {
  const reporter: TtyReporter = new TtyReporter(ttyStream);
  // tslint:disable-next-line:no-void-expression
  return (diagnostics: ReadonlyArray<Diagnostic>) => reporter.report(diagnostics);
}

class TtyReporter {
  readonly ttyStream: stream.Writable;

  constructor(ttyStream: stream.Writable) {
    this.ttyStream = ttyStream;
  }

  report(diagnostics: ReadonlyArray<Diagnostic>): void {
    for (const diagnostic of diagnostics) {
      this.ttyStream.write(Buffer.from(format(diagnostic)));
    }
  }
}

function format(diagnostic: Diagnostic): string {
  const chunks: string[] = [];
  if (diagnostic.fileUri !== undefined) {
    chunks.push(diagnostic.fileUri);
    chunks.push(": ");
  }
  chunks.push(diagnostic.message);
  chunks.push("\n");
  return chunks.join("");
}
