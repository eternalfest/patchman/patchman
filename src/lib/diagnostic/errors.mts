import { ANY_ERROR, DOX_ERROR, HAXE_ERROR, HAXE_MESSAGE } from "./codes.mjs";
import { Diagnostic, DiagnosticCategory } from "./report.mjs";

export class HaxeMessage implements Diagnostic {
  code: typeof HAXE_MESSAGE;
  category: DiagnosticCategory.Message;
  message: string;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  constructor(message: string) {
    this.message = message;
    this.code = HAXE_MESSAGE;
    this.category = DiagnosticCategory.Message;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;
  }
}

export class AnyError extends Error implements Diagnostic {
  code: typeof ANY_ERROR;
  category: DiagnosticCategory.Error;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  constructor(message: string) {
    super(message);
    this.code = ANY_ERROR;
    this.category = DiagnosticCategory.Error;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;
  }
}

export class HaxeError extends Error implements Diagnostic {
  code: typeof HAXE_ERROR;
  category: DiagnosticCategory.Error;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  constructor(message: string) {
    super(message);
    this.code = HAXE_ERROR;
    this.category = DiagnosticCategory.Error;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;
  }
}

export class DoxError extends Error implements Diagnostic {
  code: typeof DOX_ERROR;
  category: DiagnosticCategory.Error;
  exitCode: number | undefined;

  constructor(message: string, exitCode?: number) {
    super(message);
    this.code = DOX_ERROR;
    this.category = DiagnosticCategory.Error;
    this.exitCode = exitCode;
  }
}
