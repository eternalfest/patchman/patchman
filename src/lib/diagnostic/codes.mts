export const ANY_ERROR: 1 = 1;
export const HAXE_MESSAGE: 1000 = 1000;
export const HAXE_ERROR: 1001 = 1001;
export const DOX_ERROR: 1002 = 1002;
