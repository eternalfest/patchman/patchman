import assert from "assert";
import * as rx from "rxjs";
import * as rxOp from "rxjs/operators";
import url from "url";
import urlJoin from "url-join";
import yargs, {Argv} from "yargs";

import {
  createTtyReporter,
  Diagnostic,
  DiagnosticCategory,
  DiagnosticReporter,
} from "./diagnostic/report.mjs";
import { Project } from "./project.mjs";
import { VERSION } from "./version.mjs";
import { lastValueFrom } from "rxjs";
import { Buffer } from "buffer";

export type ConfigActionKind = "build" | "idea" | "doc";

const DEFAULT_WATCH: boolean = false;
const DEFAULT_IDEA: boolean = false;
const DEFAULT_DOC: boolean = false;
const DEFAULT_CHECK: boolean = false;
const DEFAULT_VERBOSE: boolean = false;

export interface ResolvedConfig {
  readonly projectRoot: url.URL;
  readonly outFile: string | undefined;
  readonly watch: boolean;
  /**
   * Only check types; don't generate any SWF file,
   * and don't run the obfuscation pass.
   */
  readonly checkOnly: boolean;
  readonly verbose: boolean;
  readonly actionKind: ConfigActionKind;
}

export type Action = MessageAction | RunAction;

export interface MessageAction {
  readonly action: "message";
  readonly message: string;
  readonly error?: Error;
}

export interface RunAction {
  readonly action: "run";
  readonly config: ResolvedConfig;
}

export type CliAction = MessageAction | CliRunAction;

export interface CliRunAction {
  readonly action: "run";
  readonly config: CliConfig;
}

export interface CliConfig {
  readonly projectRoot?: string;
  readonly outFile?: string;
  readonly checkOnly?: boolean;
  readonly verbose?: boolean;
  readonly watch: boolean;
  readonly actionKind: ConfigActionKind;
}

// TODO: Fix yargs type definition
const ARG_PARSER: Argv = (yargs as any)() as any;

ARG_PARSER
  .scriptName("patchman")
  .version(VERSION)
  .usage("$0 [opts] [project]")
  .locale("en")
  .pkgConf("patchman")
  .epilog("by Eternalfest")
  .option("out-file", {
    alias: "o",
    describe: "output file URL",
    default: undefined,
  })
  .option("watch", {
    describe: "use watch mode",
    default: DEFAULT_WATCH,
    type: "boolean",
  })
  .option("check", {
    describe: "check Haxe code without compiling",
    default: DEFAULT_CHECK,
    type: "boolean",
  })
  .option("idea", {
    describe: "generate idea config",
    default: DEFAULT_IDEA,
    type: "boolean",
  })
  .option("doc", {
    describe: "generate documentation",
    default: DEFAULT_DOC,
    type: "boolean",
  })
  .option("verbose", {
    describe: "print extra information",
    default: DEFAULT_VERBOSE,
    type: "boolean",
  })
  .check((argv: any, _options: unknown) => {
    if (argv.doc === true && argv.idea === true) {
      throw new Error("You must specify only one of --doc and --idea");
    }
    return true;
  });

/**
 * Executes the patchman CLI
 *
 * @param args CLI arguments
 * @param cwd Current working directory
 * @param proc Current process
 */
export async function execCli(args: string[], cwd: url.URL, proc: NodeJS.Process): Promise<number> {
  const action: Action = await getAction(args, cwd);

  switch (action.action) {
    case "message":
      proc.stderr.write(Buffer.from(`${action.message}\n`));
      return action.error === undefined ? 0 : 1;
    case "run":
      return execRunAction(action.config, proc);
    default:
      throw new Error(`AssertionError: Unexpected \`action\`: ${(action as any).action}`);
  }
}

export async function getAction(args: string[], cwd: url.URL): Promise<Action> {
  const parsed: CliAction = parseArgs(args);
  if (parsed.action !== "run") {
    return parsed;
  }
  return {
    action: "run",
    config: resolveConfig(cwd, parsed.config),
  };
}

export function parseArgs(args: string[]): CliAction {
  // The yargs pure API is kinda strange to use (apart from requiring a callback):
  // The error can either be defined, `undefined` or `null`.
  // If it is defined or `null`, then `output` should be a non-empty string
  // intended to be written to stderr. `parsed` is defined but it should be
  // ignored in this case.
  // If `err` is `undefined`, then `output` is an empty string and `parsed`
  // contains the succesfully parsed args.
  // tslint:disable:variable-name
  let _err: Error | undefined | null;
  let _parsed: any;
  let _output: string;
  let isParsed: boolean = false;
  ARG_PARSER.parse(args, (err: Error | undefined | null, parsed: any, output: string): void => {
    _err = err;
    _parsed = parsed;
    _output = output;
    isParsed = true;
  });
  assert(isParsed);
  const err: Error | undefined | null = _err!;
  const parsed: any = _parsed!;
  const output: string = _output!;
  if (err === null) {
    // Successfully parsed

    return {
      action: "run",
      config: {
        outFile: parsed.DEFAULT_OUT_FILE,
        // projectRoot: parsed._.length > 0 ? parsed[0] : undefined,
        watch: parsed.watch,
        verbose: parsed.verbose,
        checkOnly: parsed.check,
        actionKind: parsed.idea ? "idea" : (parsed.doc ? "doc" : "build"),
      },
    };
  } else {
    return {action: "message", message: output, error: err};
  }
}

function resolveConfig(cwd: url.URL, cliConfig: CliConfig): ResolvedConfig {
  return {
    projectRoot: cwd,
    outFile: cliConfig.outFile !== undefined ? urlJoin(cwd.toString(), cliConfig.outFile) : undefined,
    watch: cliConfig.watch ?? DEFAULT_WATCH,
    verbose: cliConfig.verbose ?? DEFAULT_VERBOSE,
    checkOnly: cliConfig.checkOnly ?? DEFAULT_CHECK,
    actionKind: cliConfig.actionKind,
  };
}

async function execRunAction(config: ResolvedConfig, proc: NodeJS.Process): Promise<number> {
  if (config.watch) {
    proc.stderr.write(Buffer.from("Using watch mode\n"));
  }
  const project: Project = await Project.fromCli(config);

  let outData$: rx.Observable<Diagnostic[]>;
  switch (config.actionKind) {
    case "build":
      outData$ = project.observeCompilation(config.checkOnly);
      break;
    case "doc":
      outData$ = project.observeDocumentation();
      break;
    case "idea":
      outData$ = project.observeIdea();
      break;
  }

  let exitCode$: rx.Observable<number> = outData$
    .pipe(rxOp.flatMap(outData => rx.from(onOutData(outData))));

  if (!config.watch) {
    exitCode$ = exitCode$.pipe(rxOp.first());
  }
  return lastValueFrom(exitCode$);

  async function onOutData(diags: Diagnostic[]): Promise<number> {
    try {
      const hasErrors: boolean = diags.some(d => d.category === DiagnosticCategory.Error);
      const reporter: DiagnosticReporter = createTtyReporter(proc.stderr);
      reporter(diags);
      const msg: string = `Haxe compilation ${hasErrors ? "failed" : "successful"}.\n`;
      proc.stderr.write(Buffer.from(msg));
      return hasErrors ? 1 : 0;
    } catch (err) {
      if (err instanceof Error) {
        proc.stderr.write(Buffer.from(`InternalCompilerError:\n${err.stack}\n`));
      } else {
        proc.stderr.write(Buffer.from(`InternalCompilerError:\n${err}\n`));
      }
      return 1;
    }
  }
}
