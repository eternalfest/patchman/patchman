import * as loader from "@eternalfest-types/loader";
import obf from "@eternalfest/obf/obf.js";
import fs from "fs";
import url from "url";

import { outputFileAsync, readFile } from "./fs-utils.mjs";

export async function obfuscate(
  inputFile: url.URL,
  outputFile: url.URL,
  mapFile: url.URL,
  key: string | null,
): Promise<void> {
  const as2Map: Map<string, string> = await obf.getAs2Map();
  const rawLoaderMap: any = JSON.parse(fs.readFileSync(loader.getObfuMapUri(), {encoding: "utf-8"}));
  const loaderMap: Map<string, string> = obf.asMap(rawLoaderMap);
  const parentMap: Map<string, string> = obf.mergeMaps(as2Map, loaderMap);

  const inBytes: Uint8Array = await readFile(inputFile);
  const result: any = obf.obfuscateBytes(inBytes, key, parentMap);
  const mapRecord: Record<string, string> = Object.create(null);
  for (const [newStr, oldStr] of result.map) {
    Reflect.set(mapRecord, newStr, oldStr);
  }
  await Promise.all([
    outputFileAsync(outputFile, result.bytes),
    outputFileAsync(mapFile, Buffer.from(`${JSON.stringify(mapRecord, null, 2)}\n`)),
  ]);
}
