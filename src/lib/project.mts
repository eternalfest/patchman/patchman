import dox from "@eternalfest/haxe-dox";
import { PackageJson, PatchmanDependencyGraph, resolvePatchmanDependencies as resolveDeps, testPackageUniqueness } from "@eternalfest/patchman-resolver";
import { findUp } from "find-up";
import furi from "furi";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import * as rx from "rxjs";
import * as rxOp from "rxjs/operators";
import semver from "semver";
import tmp from "tmp-promise";
import url from "url";
import urlJoin from "url-join";

import { ResolvedConfig as ResolvedCliConfig } from "./cli.mjs";
import { DoxError } from "./diagnostic/errors.mjs";
import { Diagnostic, DiagnosticCategory } from "./diagnostic/report.mjs";
import { deleteDir,mkdirpAsync, readDir, readTextFile } from "./fs-utils.mjs";
import { buildTargetToHxml, executeHxml, HaxeBuildTarget, HaxeHxml, HaxeXmlTarget, writeHxml, xmlTargetToHxml } from "./node-haxe/haxe.mjs";
import { obfuscate } from "./obfu.mjs";
import {
  $PackageJsonConfig,
  PackageJsonConfig,
  PackageResolution,
  ProjectConfig,
} from "./project-config.mjs";
import { LibraryFile, outputLibraryFile, updateIml } from "./project-idea.mjs";
import { getHaxeStdLibUrl } from "./std-lib.mjs";
import { VERSION } from "./version.mjs";

const DEFAULT_OUT_FILE: string = "patchman.swf";
const DEFAULT_OUT_DOC_DIR: string = "docs";
const DEFAULT_MAIN: string = "Main";

export class Project {
  private readonly base: url.URL;
  private readonly config: ProjectConfig;
  private readonly verbose: boolean;

  private constructor(base: url.URL, verbose: boolean, config: ProjectConfig) {
    this.base = base;
    this.verbose = verbose;
    this.config = config;
  }

  public static async fromCli(cliConfig: ResolvedCliConfig): Promise<Project> {
    const base: url.URL = cliConfig.projectRoot;
    const fileConfig: PackageJsonConfig | undefined = await findConfigFromFile(base);
    const baseMain: string = fileConfig !== undefined && fileConfig.haxeMain !== undefined
      ? fileConfig.haxeMain
      : DEFAULT_MAIN;
    const baseOutFile: string = fileConfig !== undefined && fileConfig.outFile !== undefined
      ? fileConfig.outFile
      : DEFAULT_OUT_FILE;
    const outDocDir: string = fileConfig !== undefined && fileConfig.outDocDir !== undefined
      ? fileConfig.outDocDir
      : DEFAULT_OUT_DOC_DIR;
    // tslint:disable-next-line:max-line-length
    const basePackageResolution: PackageResolution = fileConfig !== undefined && fileConfig.packageResolution !== undefined
      ? fileConfig.packageResolution
      : PackageResolution.Node;

    const outFile: string = cliConfig.outFile !== undefined ? cliConfig.outFile : baseOutFile;
    const obfuMap: string = fileConfig !== undefined && fileConfig.obfuMap !== undefined
      ? fileConfig.obfuMap
      : baseOutFile.replace(/(?:\.swf)$/, ".map.json");

    const config: ProjectConfig = {
      main: baseMain,
      outFile,
      outDocDir,
      obfuMap,
      obfuKey: "TODO: Do not use a hardcoded key",
      packageResolution: basePackageResolution,
    };
    return new Project(base, cliConfig.verbose, config);
  }

  public static fromConfig(base: url.URL, verbose: boolean, config: ProjectConfig): Project {
    return new Project(base, verbose, config);
  }

  public getOutFile(): url.URL {
    try {
      return new url.URL(this.config.outFile);
    } catch (e) {
      return new url.URL(urlJoin(this.base.toString(), this.config.outFile));
    }
  }

  public getOutDocDir(): url.URL {
    try {
      return new url.URL(this.config.outDocDir);
    } catch (e) {
      return new url.URL(urlJoin(this.base.toString(), this.config.outDocDir));
    }
  }

  public async resolveHaxeBuildTarget(checkOnly: boolean = false): Promise<HaxeBuildTarget> {
    const deps: readonly ResolvedPatchmanDependency[] = await resolvePatchmanDependencies(this.base);
    return {
      main: this.config.main,
      classPath: getClasspathsForDependencies(deps),
      defines: getDefinesForDependencies(deps),
      outFile: this.getOutFile(),
      noOutput: checkOnly,
      swfHeader: "420:520:40:3f3f3f",
    };
  }

  observeCompilation(checkOnly: boolean = false): rx.Observable<Diagnostic[]> {
    const hxTarget$: rx.Observable<HaxeBuildTarget> = rx.from(this.resolveHaxeBuildTarget(checkOnly));
    return hxTarget$
      .pipe(
        rxOp.flatMap(async (hxTarget: HaxeBuildTarget): Promise<Diagnostic[]> => {
          const hxml = buildTargetToHxml(hxTarget);
          this.printHxmlVerbose(hxml);
          mkdirpAsync(furi.parent(hxTarget.outFile));
          const diags: Diagnostic[] = await executeHxml(hxml);
          const hasError: boolean = diags.some(d => d.category === DiagnosticCategory.Error);
          if (hasError) {
            return diags;
          }
          if (!hxTarget.noOutput) {
            await obfuscate(hxTarget.outFile, hxTarget.outFile, new url.URL(this.config.obfuMap), this.config.obfuKey);
          }
          return diags;
        }),
      );
  }

  async compile(): Promise<Diagnostic[]> {
    return rx.firstValueFrom(this.observeCompilation());
  }

  public async resolveHaxeXmlTarget(outFile: url.URL): Promise<HaxeXmlTarget> {
    const partial = await this.resolveHaxeXmlTargetPartial();
    return { outFile, ...partial };
  }

  private async resolveHaxeXmlTargetPartial(): Promise<Omit<HaxeXmlTarget, "outFile">> {
    const deps: readonly ResolvedPatchmanDependency[] = await resolvePatchmanDependencies(this.base);
    const classPath: url.URL[] = getClasspathsForDependencies(deps);

    const includes: Set<string> = new Set();
    for (const cp of classPath) {
      for (const pack of await readDir(cp, "dirs")) {
        includes.add(pack);
      }
    }

    return {
      main: this.config.main,
      classPath,
      defines: getDefinesForDependencies(deps),
      packIncludes: [...includes].sort(),
    };
  }

  observeDocumentation(): rx.Observable<Diagnostic[]> {
    const hxTarget$ = rx.from(this.resolveHaxeXmlTargetPartial());
    return hxTarget$
      .pipe(
        rxOp.flatMap(async (hxTarget): Promise<Diagnostic[]> => tmp.withDir(async tmpDir => {
          const docDir = this.getOutDocDir();
          const xmlFile: url.URL = furi.join(furi.fromSysPath(tmpDir.path), "doc-swf.xml");
          await deleteDir(docDir);
          await mkdirpAsync(docDir);

          const hxml = xmlTargetToHxml({ outFile: xmlFile, ...hxTarget });
          this.printHxmlVerbose(hxml);
          const diags = await executeHxml(hxml);

          const hasError: boolean = diags.some(d => d.category === DiagnosticCategory.Error);
          if (hasError) {
            return diags;
          }

          return diags.concat(diagnoseDoxResult(await dox.runDox({
            inputPath: tmpDir.path,
            outputPath: furi.toSysPath(docDir),
          })));
        }, { unsafeCleanup: true }))
      );
  }

  async document(): Promise<Diagnostic[]> {
    return rx.firstValueFrom(this.observeDocumentation());
  }

  observeIdea(): rx.Observable<Diagnostic[]> {
    return rx.from(this.ejectIdea());
  }

  async ejectIdea(): Promise<Diagnostic[]> {
    const packages: ReadonlyArray<ResolvedPatchmanDependency> = await resolvePatchmanDependencies(this.base);
    const libNames: string[] = [];
    for (const pkg of packages) {
      if (this.base.toString() === pkg.url.toString()) {
        continue;
      } else if (pkg.haxe.classPath.length === 0) {
        continue;
      }
      const lib: LibraryFile = await outputLibraryFile(this.base, pkg.name, pkg.haxe.classPath);
      libNames.push(lib.id);
    }
    libNames.sort();
    await updateIml(this.base, libNames);
    return [];
  }

  private printHxmlVerbose(hxml: HaxeHxml) {
    if (this.verbose) {
      console.warn(writeHxml(hxml, this.base));
    }

  }
}

interface ResolvedPatchmanDependency {
  readonly url: url.URL;
  readonly name: string;
  readonly version: string;
  readonly haxe: PackageHaxeConfig;
  readonly dependencies: ReadonlyMap<string, string>;
  readonly devDependencies: ReadonlyMap<string, string>;
  readonly peerDependencies: ReadonlyMap<string, string>;
}

interface PackageHaxeConfig {
  readonly classPath: ReadonlyArray<url.URL>;
}

function getClasspathsForDependencies(deps: ReadonlyArray<ResolvedPatchmanDependency>): url.URL[] {
  const patchmanStd: url.URL = getHaxeStdLibUrl();

  const seen: Set<string> = new Set([patchmanStd.toString()]);
  const classPath: url.URL[] = [patchmanStd];

  for (const pkg of deps) {
    for (const cp of pkg.haxe.classPath) {
      const cpStr: string = cp.toString();
      if (!seen.has(cpStr)) {
        seen.add(cpStr);
        classPath.push(cp);
      }
    }
  }

  return classPath;
}

const PATCHMAN_DEPENDENCY: RegExp = /^@patchman\/(\S+)$/;

function getDefinesForDependencies(deps: ReadonlyArray<ResolvedPatchmanDependency>): Map<string, string> {
  const defines: Map<string, string> = new Map();
  // Core patchman version.
  defines.set("patchman", VERSION);

  for (const pkg of deps) {
    // Don't emit a 'define' for non-patchman dependencies.
    const match: RegExpExecArray | null = PATCHMAN_DEPENDENCY.exec(pkg.name);
    if (match === null) {
      continue;
    }

    // Make name of define: `@patchman/foo-bar` becomes `foo_bar`.
    const name = stringReplaceAll(match[1], "-", "_");

    defines.set(name, pkg.version);
  }

  return defines;
}

// Polyfill for String.replaceAll, which doesn't exist in node 14.
function stringReplaceAll(str: string, find: string, replace: string): string {
  return str.split(find).join(replace);
}

async function resolvePatchmanDependencies(base: url.URL): Promise<ReadonlyArray<ResolvedPatchmanDependency>> {
  const result: ResolvedPatchmanDependency[] = [];
  const depGraph: PatchmanDependencyGraph = await resolveDeps(base);
  testPackageUniqueness(depGraph);
  testEngineCompat(depGraph);
  for (const pkg of depGraph.getPackages()) {
    result.push({
      url: pkg.uri,
      name: pkg.name,
      version: pkg.package.version,
      haxe: readHaxeConfig(pkg.uri, pkg.package),
      dependencies: new Map(),
      devDependencies: new Map(),
      peerDependencies: new Map(),
    });
  }
  return result;
}

function readHaxeConfig(dirUrl: url.URL, packageJson: PackageJson): PackageHaxeConfig {
  const rawPatchmanData: unknown = Reflect.get(packageJson, "patchman");
  let patchmanData: object;
  if (rawPatchmanData === undefined) {
    // throw new Error(`InvalidPackageJson: Missing "patchman" field: ${packageJsonUrl}`);
    patchmanData = {};
  } else if (typeof rawPatchmanData !== "object" || rawPatchmanData === null) {
    throw new Error("InvalidPackageJson: Not a document (\"patchman\")");
  } else {
    patchmanData = rawPatchmanData;
  }
  const rawHaxeData: unknown = Reflect.get(patchmanData, "haxe");
  const classPath: url.URL[] = [];
  if (rawHaxeData === undefined) {
    classPath.push(dirUrl);
  } else if (typeof rawHaxeData === "string") {
    classPath.push(new url.URL(urlJoin(dirUrl.toString(), rawHaxeData)));
  } else if (Array.isArray(rawHaxeData)) {
    for (const rawHaxeDataElem of rawHaxeData) {
      if (typeof rawHaxeDataElem === "string") {
        classPath.push(new url.URL(urlJoin(dirUrl.toString(), rawHaxeDataElem)));
      } else {
        throw new Error("InvalidPackageJson: Not an array of strings (\"patchman.haxe\")");
      }
    }
  } else {
    throw new Error("InvalidPackageJson: Not a string or an array of strings (\"patchman.haxe\")");
  }
  return {classPath};
}

async function findConfigFromFile(base: url.URL): Promise<PackageJsonConfig | undefined> {
  const pkgJsonPath: string | undefined = await findUp("package.json", {cwd: url.fileURLToPath(base)});
  if (pkgJsonPath === undefined) {
    return undefined;
  }
  const packageJsonUrl: url.URL = url.pathToFileURL(pkgJsonPath);
  const packageJsonStr: string = await readTextFile(packageJsonUrl);
  const packageJson: unknown = JSON.parse(packageJsonStr);
  if (typeof packageJson !== "object" || packageJson === null) {
    throw new Error(`InvalidPackageJson: Not a document: ${packageJsonUrl}`);
  }
  const rawPatchmanData: unknown = Reflect.get(packageJson, "patchman");
  if (rawPatchmanData === undefined) {
    return undefined;
  } else if (typeof rawPatchmanData !== "object" || rawPatchmanData === null) {
    throw new Error(`InvalidPackageJson: Not a document ("patchman"): ${packageJsonUrl}`);
  } else {
    const config: PackageJsonConfig = $PackageJsonConfig.read(JSON_VALUE_READER, rawPatchmanData);
    if (config.outFile !== undefined) {
      config.outFile = new url.URL(config.outFile, packageJsonUrl).toString();
    }
    return config;
  }
}

function testEngineCompat(graph: PatchmanDependencyGraph): void {
  for (const pkg of graph.getPackages()) {
    const engineRange: string | undefined = readPatchmanEngine(pkg.package);
    if (engineRange === undefined) {
      continue;
    }
    if (!semver.satisfies(VERSION, engineRange)) {
      throw new Error(`IncompatiblePatchmanVersion(${pkg.name}): Version: ${VERSION}, Range: ${engineRange}`);
    }
  }
}

function readPatchmanEngine(packageJson: PackageJson): string | undefined {
  const rawEngines: unknown = Reflect.get(packageJson, "engines");
  if (rawEngines === undefined) {
    return undefined;
  } else if (typeof rawEngines !== "object" || rawEngines === null) {
    throw new Error(`InvalidPackageJson(${packageJson.name}): Not a document ("engines")`);
  }
  const engines: Record<string, unknown> = rawEngines as Record<string, unknown>;
  const rawPatchmanEngine: unknown = Reflect.get(engines, "patchman");
  if (rawPatchmanEngine === undefined) {
    return undefined;
  } else if (typeof rawPatchmanEngine !== "string") {
    throw new Error(`InvalidPackageJson(${packageJson.name}): Not a string ("engines.patchman")`);
  } else if (!semver.validRange(rawPatchmanEngine)) {
    throw new Error(`InvalidPackageJson(${packageJson.name}): Not a valid semver range ("engines.patchman")`);
  }
  return rawPatchmanEngine;
}

function diagnoseDoxResult(doxResult: dox.DoxResult): Diagnostic[] {
  switch (doxResult.type) {
    case "success":
      return [];
    case "error":
      return [new DoxError(doxResult.message, doxResult.code.valueOf())];
    case "interrupted":
      return [new DoxError(`Dox was unexpectedly interrupted (${doxResult.signal})`)];
  }
}
