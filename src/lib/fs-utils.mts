import fs from "fs";
import fsPromises from "fs/promises";
import furi from "furi";
import sysPath from "path";
import url from "url";

export async function outputFileAsync(filePath: url.URL, data: Uint8Array): Promise<void> {
  await mkdirpAsync(furi.fromSysPath(sysPath.dirname(furi.toSysPath(filePath.toString()))));
  await writeFileAsync(filePath, data);
}

export async function mkdirpAsync(dirPath: url.URL): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.mkdir(dirPath, {recursive: true}, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function writeFileAsync(filePath: url.URL, data: Uint8Array): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function readFile(filePath: url.URL): Promise<Uint8Array> {
  return new Promise<Uint8Array>((resolve, reject): void => {
    fs.readFile(filePath, {encoding: null}, (err: NodeJS.ErrnoException | null, data: Uint8Array): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

export async function readTextFile(filePath: url.URL): Promise<string> {
  return new Promise<string>((resolve, reject): void => {
    fs.readFile(filePath, {encoding: "utf-8"}, (err: NodeJS.ErrnoException | null, text: string): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(text);
      }
    });
  });
}

export async function readDir(dirPath: url.URL, filter?: "dirs" | "files"): Promise<string[]> {
  const dir: string = furi.toSysPath(dirPath);
  switch (filter) {
    case undefined:
      return await fsPromises.readdir(dir);
    case "dirs": {
      const dirs = [];
      for (const ent of await fsPromises.readdir(dir, { withFileTypes: true })) {
        if (ent.isDirectory()) {
          dirs.push(ent.name);
        }
      }
      return dirs;
    }
    case "files": {
      const files = [];
      for (const ent of await fsPromises.readdir(dir, { withFileTypes: true })) {
        if (ent.isFile()) {
          files.push(ent.name);
        }
      }
      return files;
    }
  }
}

export async function fileExists(filePath: fs.PathLike): Promise<boolean> {
  return new Promise<boolean>((resolve, reject): void => {
    fs.stat(filePath, (err: NodeJS.ErrnoException | null, stats: fs.Stats): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(stats.isFile());
      }
    });
  });
}

export async function deleteDir(dirPath: url.URL): Promise<void> {
  try {
    return await fsPromises.rmdir(dirPath, {
      recursive: true,
    });
  } catch (e: any) {
    if (e.code !== "ENOENT") {
      throw e;
    }
  }
}
