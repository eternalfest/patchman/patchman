import cheerio from "cheerio";
import fs from "fs";
import sysPath from "path";
import url from "url";
import urlJoin from "url-join";

import { outputFileAsync, readTextFile } from "./fs-utils.mjs";

export interface LibraryFile {
  id: string;
  shortName: string;
  safeName: string;
  data: string;
  file: url.URL;
}

export function prepareLibraryFile(
  projectDir: url.URL,
  libId: string,
  classPaths: ReadonlyArray<url.URL>,
): LibraryFile {
  const shortName: string = libId.replace(/^@patchman\//, "");
  const safeName: string = libId.replace(/[^a-zA-Z0-9]/g, "_");

  function toUrl(cp: url.URL): string {
    return cp.toString().startsWith(projectDir.toString())
      ? `file://$PROJECT_DIR$${cp.toString().substring(projectDir.toString().length)}`
      : cp.toString();
  }

  const cpRows: string[] = [];
  for (const cp of classPaths) {
    cpRows.push(`      <root url=${JSON.stringify(toUrl(cp))} />`);
  }

  const data: string = [
    "<component name=\"libraryTable\">",
    `  <library name=${JSON.stringify(libId)}>`,
    "    <CLASSES>",
    ...cpRows,
    "    </CLASSES>",
    "    <JAVADOC />",
    "    <SOURCES>",
    ...cpRows,
    "    </SOURCES>",
    "  </library>",
    "</component>",
    "",
  ].join("\n");

  const file: url.URL = new url.URL(urlJoin(projectDir.toString(), ".idea", "libraries", `${safeName}.xml`));

  return {id: libId, shortName, safeName, data, file};
}

export async function outputLibraryFile(
  projectDir: url.URL,
  libId: string,
  classPaths: ReadonlyArray<url.URL>,
): Promise<LibraryFile> {
  const lib: LibraryFile = prepareLibraryFile(projectDir, libId, classPaths);
  await outputFileAsync(lib.file, Buffer.from(lib.data));
  return lib;
}

export async function updateIml(projectDir: url.URL, libraries: ReadonlyArray<string>): Promise<void> {
  let iml: ImlFile = await findImlFile(projectDir);
  iml = enableLibraries(iml, libraries);
  iml = ensureHaxe(iml);
  if (iml.data !== undefined) {
    return outputFileAsync(iml.url, Buffer.from(iml.data));
  }
}

export function enableLibraries(iml: ImlFile, libraries: ReadonlyArray<string>): ImlFile {
  if (iml.data === undefined) {
    return iml;
  }

  const $: cheerio.Root = cheerio.load(iml.data, {xmlMode: true});
  const moduleNode: cheerio.Cheerio = $("module");
  const rootManager: cheerio.Cheerio = moduleNode.find("component[name=\"NewModuleRootManager\"]");
  if (rootManager.length !== 1) {
    throw new Error("Expected 1 node: `component[name=\"NewModuleRootManager\"]`");
  }
  for (const lib of libraries) {
    if (hasLib(lib)) {
      continue;
    }
    const entry: cheerio.Cheerio = $("<orderEntry />")
      .attr("type", "library")
      .attr("name", lib)
      .attr("level", "project");
    rootManager.append(entry);
  }
  function hasLib(lib: string): boolean {
    return rootManager.find(`orderEntry[type="library"][name=${JSON.stringify(lib)}]`).length > 0;
  }
  return {...iml, data: $.xml()};
}

export function ensureHaxe(iml: ImlFile): ImlFile {
  if (iml.data === undefined) {
    return iml;
  }

  const $: cheerio.Root = cheerio.load(iml.data, {xmlMode: true});
  const moduleNode: cheerio.Cheerio = $("module");
  if (moduleNode.attr("type") === "HAXE_MODULE") {
    return iml;
  }
  const rootManager: cheerio.Cheerio = moduleNode.find("component[name=\"NewModuleRootManager\"]");
  if (rootManager.length !== 1) {
    throw new Error("Expected 1 node: `component[name=\"NewModuleRootManager\"]`");
  }
  moduleNode.attr("type", "HAXE_MODULE");

  moduleNode.append(`
    <component name="HaxeModuleSettingsStorage">
      <option name="buildConfig" value="1" />
      <option name="haxeTarget" value="Interpretation" />
      <option name="hxmlPath" value="$MODULE_DIR$/patchman.hxml" />
      <option name="outputFolder" value="$MODULE_DIR$/build/patchman" />
    </component>
  `);

  const buildDir: string = "file://$MODULE_DIR$/build";
  if (!hasOutput(buildDir)) {
    const output: cheerio.Cheerio = $("<output />")
      .attr("url", "buildDir");
    rootManager.append(output);
  }
  function hasOutput(url: string): boolean {
    return rootManager.find(`output[url=${JSON.stringify(url)}]`).length > 0;
  }
  return {...iml, data: $.xml()};
}

export interface ImlFile {
  url: url.URL;
  data?: string;
}

export async function findImlFile(projectDir: url.URL): Promise<ImlFile> {
  const projectDirPath: string = url.fileURLToPath(projectDir);
  const projectName: string = sysPath.basename(projectDirPath);
  const projectNameIml: string = `${projectName}.iml`;
  const ideaDirPath: string = sysPath.join(projectDirPath, ".idea");

  const ideaFiles: string[] = await findImlFiles(ideaDirPath);
  let imlPath: string;
  if (ideaFiles.indexOf(projectNameIml) >= 0) {
    imlPath = sysPath.join(ideaDirPath, projectNameIml);
  } else if (ideaFiles.length > 0) {
    imlPath = sysPath.join(ideaDirPath, ideaFiles[0]);
  } else {
    const projectFiles: string[] = await findImlFiles(projectDirPath);
    if (projectFiles.indexOf(projectNameIml) >= 0) {
      imlPath = sysPath.join(projectDirPath, projectNameIml);
    } else if (projectFiles.length > 0) {
      imlPath = sysPath.join(projectDirPath, projectFiles[0]);
    } else {
      imlPath = sysPath.join(ideaDirPath, projectNameIml);
      return {url: url.pathToFileURL(imlPath)};
    }
  }
  const imlUrl: url.URL = url.pathToFileURL(imlPath);
  const data: string = await readTextFile(imlUrl);
  return {url: imlUrl, data};

  async function findImlFiles(dirPath: string): Promise<string[]> {
    const files: fs.Dirent[] = await new Promise((resolve, reject) => {
      fs.readdir(dirPath, {withFileTypes: true}, (err: Error | null, files: fs.Dirent[]) => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(files);
        }
      });
    });
    const imlFiles: string[] = [];
    for (const file of files) {
      if (file.isFile() && file.name.endsWith(".iml")) {
        imlFiles.push(file.name);
      }
    }
    return imlFiles;
  }
}
