package main;

import ef.patcher.ConsoleModule;
import ef.patcher.GameEngine;
import nolife.Nolife;
import patchman.Patchman;

@:rtti
class Main {
  public static function main():Void {
    Patchman.bootstrap(Main);
  }

  public function new(engine:GameEngine, nolife:Nolife) {
    nolife.patch(engine);
  }
}
