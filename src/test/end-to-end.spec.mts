import chai from "chai";
import furi from "furi";
import sysPath from "path";
import url from "url";

import { buildTargetToHxml, HaxeBuildTarget, HaxeXmlTarget, writeHxml,xmlTargetToHxml } from "../lib/node-haxe/haxe.mjs";
import { Project } from "../lib/project.mjs";
import { VERSION } from "../lib/version.mjs";
import meta from "./meta.mjs";

const END_TO_END_DIR: string = sysPath.join(meta.dirname, "end-to-end");

describe("end-to-end", function () {
  it("deps-build", async function () {
    const baseDir: url.URL = furi.fromSysPath(sysPath.join(END_TO_END_DIR, "deps"));
    const project: Project = Project.fromConfig(baseDir, false, {
      main: "main.Main",
      outFile: "patchman.swf",
      outDocDir: "docs",
      obfuMap: "patchman.map.json",
      obfuKey: "TODO",
    });
    const haxeTarget: HaxeBuildTarget = await project.resolveHaxeBuildTarget();
    const actualHxml: string = writeHxml(buildTargetToHxml(haxeTarget), baseDir);
    const expectedHxml: string = [
      "-main \"main.Main\"",
      "-cp \"../../../haxe\"",
      "-cp \"src\"",
      "-cp \"node_modules/@patchman/nolife/src\"",
      `-D patchman=${VERSION}`,
      "-D nolife=0.0.0",
      "-swf-version \"8\"",
      "-swf-header \"420:520:40:3f3f3f\"",
      "-swf \"patchman.swf\"",
      "-D flash_strict",
      "-D obfu",
      "-dce full",
      "--no-traces",
      "",
    ].join("\n");
    chai.assert.strictEqual(actualHxml, expectedHxml);
  });

  it("deps-xml", async function () {
    const baseDir: url.URL = furi.fromSysPath(sysPath.join(END_TO_END_DIR, "deps"));
    const project: Project = Project.fromConfig(baseDir, false, {
      main: "main.Main",
      outFile: "patchman.swf",
      outDocDir: "docs",
      obfuMap: "patchman.map.json",
      obfuKey: "TODO",
    });

    const outFile: url.URL = furi.fromSysPath(sysPath.join(END_TO_END_DIR, "deps", "patchman-swf.xml"));
    const haxeTarget: HaxeXmlTarget = await project.resolveHaxeXmlTarget(outFile);
    const actualHxml: string = writeHxml(xmlTargetToHxml(haxeTarget), baseDir);
    const expectedHxml: string = [
      "-main \"main.Main\"",
      "-cp \"../../../haxe\"",
      "-cp \"src\"",
      "-cp \"node_modules/@patchman/nolife/src\"",
      `-D patchman=${VERSION}`,
      "-D nolife=0.0.0",
      "--macro include(\"ef\")",
      "--macro include(\"etwin\")",
      "--macro include(\"hf\")",
      "--macro include(\"nolife\")",
      "--macro include(\"patchman\")",
      "-swf-version \"8\"",
      "-swf \"dummy.swf\"",
      "-D flash_strict",
      "-D doc-gen",
      "--no-output",
      "-xml \"patchman-swf.xml\"",
      "",
    ].join("\n");
    chai.assert.strictEqual(actualHxml, expectedHxml);
  });
});
