import url from "url";

import { SpawnedProcess } from "../../lib/spawned-process.mjs";
import furi from "furi";

const PROJECT_ROOT = furi.join(import.meta.url, "..", "..", "..");
const CODEGEN_SRC = furi.join(PROJECT_ROOT, "tools", "codegen");
const CODEGEN_SRC_PATH: string = furi.toSysPath(CODEGEN_SRC);

const MAIN_PACK: string = "hf";

async function runHaxeCompiler(args: string[], env?: Record<string, string>): Promise<void> {
  console.log(args);
  let result = await new SpawnedProcess("haxe", args, { stdio: "pipe", env }).toPromise();
  let stderr = result.stderr.toString("utf-8");
  if (stderr.length > 0) {
    throw new Error("HaxeError:\n" + stderr);
  }
}

export async function generateHaxeClassXml(classPath: url.URL[], outFile: url.URL): Promise<void> {
  const args: string[] = [];
  for (const path of classPath) {
    args.push("-cp", url.fileURLToPath(path));
  }
  args.push("--macro", `include("${MAIN_PACK}")`);
  args.push("-swf-version", "8");
  args.push("-swf", "dummy");
  args.push("-D", "codegen");
  args.push("--no-output");
  args.push("-xml", url.fileURLToPath(outFile));
  await runHaxeCompiler(args);
}

export async function runHaxeCodegen(libPath: url.URL[], outDir: url.URL, xmlFile: url.URL): Promise<void> {
  const args: string[] = [];
  args.push("-main", "Codegen");
  args.push("-cp", CODEGEN_SRC_PATH);
  for (const path of libPath) {
    args.push("-cp", url.fileURLToPath(path));
  }
  args.push("--interp");
  const env = {
    CODEGEN_XML_PATH: url.fileURLToPath(xmlFile),
    CODEGEN_OUT_DIR: url.fileURLToPath(outDir),
    CODEGEN_MAIN_PACK: MAIN_PACK,
  };
  await runHaxeCompiler(args, env);
}
