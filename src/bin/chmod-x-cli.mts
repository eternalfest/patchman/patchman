import fs from "fs";
import sysPath from "path";

const PROJECT_ROOT: string = sysPath.resolve(__dirname, "..");
const BIN_PATH: string = sysPath.join(PROJECT_ROOT, "dist", "lib", "bin", "patchman.js");

const oldMode: number = fs.statSync(BIN_PATH).mode;
const newMode: number = oldMode | fs.constants.S_IXUSR | fs.constants.S_IXGRP | fs.constants.S_IXOTH;

fs.chmodSync(BIN_PATH, newMode);
