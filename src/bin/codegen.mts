import { generateHaxeClassXml, runHaxeCodegen } from "./codegen/index.mjs";
import url from "url";
import * as furi from "furi";
import sysPath from "path";
import { getHaxeTypesPath as getGameHaxeTypesPath } from "@eternalfest-types/game";
import tmp from "tmp-promise";
import {dirname} from "./meta.mjs";

const PROJECT_ROOT = sysPath.join(dirname, "..");

const GAME_HAXE_TYPES = url.pathToFileURL(getGameHaxeTypesPath());
const ETWIN_HAXE_TYPES = url.pathToFileURL(
  sysPath.join(PROJECT_ROOT, "node_modules", "@etwin-haxe", "core", "src")
);

async function codegen() {
  return tmp.withDir(async ({path}): Promise<void> => {
    const haxeXmlUrl: url.URL = furi.join(furi.fromSysPath(path), ["hf-haxe.xml"]);
    const outDirUrl: url.URL = furi.join(furi.fromSysPath(PROJECT_ROOT), ["haxe"]);
    const classPath: url.URL[] = [GAME_HAXE_TYPES, ETWIN_HAXE_TYPES];
    await generateHaxeClassXml(classPath, haxeXmlUrl);
    await runHaxeCodegen(classPath, outDirUrl, haxeXmlUrl);
  }, {unsafeCleanup: true});
}

codegen();
